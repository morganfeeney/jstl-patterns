# JSTL Patterns #

### What is this repository for? ###

This repo contains a working demonstration of how patterns, made from Bootstrap and more, can be created using JSTL .tag and .jsp files. [Example of pattern library](http://patternlab.io/).

It's a work in progress and intended as a possible starting point for discussion on how the files could be structured in order to promote re-use and to take advantage of template inheritance in order to avoid code duplication.

It also provides a way to separate the structure of a layout from that of a .jsp file containing logic.

JSTL is a templating language, the syntax shares some similarities with many other templating langauges such as nunjucks:

* [JavaServer Pages Standard Tag Library 1.1 Tag Reference](http://docs.oracle.com/javaee/5/jstl/1.1/docs/tlddocs/)
* [Detailed documentation](http://docs.oracle.com/javaee/5/tutorial/doc/bnakc.html)

### How do I get set up? ###

* You'll need to run a local [tomcat server](https://tomcat.apache.org/download-90.cgi). Choose a mirror site to download from then navigate the DIR structure to '/tomcat/<version>/bin' and choose the 'tar.gz' file. Unzip the contents into 'HD/Library/Tomcat'.
* To start the server from the Terminal use './start.sh' from the Tomcat '/bin' directory or download [tomcat Controller](http://www.activata.co.uk/tomcatcontroller/) for a GUI. If everything is running you should see the Tomcat start page at 'http://localhost:8080'
* Then clone the repo into the apps directory 'Tomcat/webapps/jstl-layouts'
* Tomcat requires a Java SE Runtime Environment. If not already installed then follow the instructions as detailed in the readme file RUNNING.txt which comes with Tomcat

### Contribution guidelines ###

* If you want to change something or have a suggestion clone the repo and submit a pull request.
