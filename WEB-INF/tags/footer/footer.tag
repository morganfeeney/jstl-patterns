<footer class="page-footer">

    <div class="container-max-width">
        <div class="ff-title">
            <h3>Get Connected</h3>
        </div>
    </div>
    <!-- / Free Format -->

    <!-- Page Footer Top -->
    <div class="page-footer-top">
        <div class="footer-container">
            <div class="row">
                <div class="block-1">

                    <!-- Email Sign Up Component -->
                    <div class="footer-email-sign-up">

                        <div class="email-success">
                            <h5 class="h4">Thank You</h5>
                            <p class="no-margin">You have succesfully signed up to our mailing list.</p>
                        </div>

                        <div class="email-current-customer">
                            <h5 class="h4">Welcome Back</h5>
                            <p class="no-margin">Please update your contact preferences to receive emails from us.</p>
                        </div>

                        <h4>Stay in the loop with news and offers</h4>
                        <div class="form-group has-error">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Email Sign Up">
                                <span class="input-group-btn">
                                    <button class="btn btn-secondary btn-sign-up" type="button">Sign Up</button>
                                </span>
                            </div>

                            <div class="alert alert-danger text-left" role="alert">

                                <div class="alert-inner">Please enter a valid email address</div>
                            </div>

                        </div>

                    </div>
                    <!-- / Email Sign Up Component -->

                </div>
                <div class="block-2">

                    <!-- Social Links Component -->
                    <div class="footer-social-links">
                        <h4>Feeling Social?</h4>
                        <a href="#" class="footer-social-icon" target="_blank">
                            <img src="//images2.drct2u.com/hybris/theme-common/images/social/svg/icon-facebook.svg" alt="Facebook">
                        </a>
                        <a href="#" class="footer-social-icon" target="_blank">
                            <img src="//images2.drct2u.com/hybris/theme-common/images/social/svg/icon-tumblr.svg" alt="Tumblr">
                        </a>
                        <a href="#" class="footer-social-icon" target="_blank">
                            <img src="//images2.drct2u.com/hybris/theme-common/images/social/svg/icon-twitter.svg" alt="Twitter">
                        </a>
                        <a href="#" class="footer-social-icon" target="_blank">
                            <img src="//images2.drct2u.com/hybris/theme-common/images/social/svg/icon-youtube.svg" alt="Youtube">
                        </a>
                        <a href="#" class="footer-social-icon" target="_blank">
                            <img src="//images2.drct2u.com/hybris/theme-common/images/social/svg/icon-googleplus.svg" alt="Google+">
                        </a>
                        <a href="#" class="footer-social-icon" target="_blank">
                            <img src="//images2.drct2u.com/hybris/theme-common/images/social/svg/icon-pinterest.svg" alt="Pinterest">
                        </a>
                        <a href="#" class="footer-social-icon" target="_blank">
                            <img src="//images2.drct2u.com/hybris/theme-common/images/social/svg/icon-instagram.svg" alt="Instagram">
                        </a>
                        <a href="#" class="footer-social-icon" target="_blank">
                            <img src="//images2.drct2u.com/hybris/theme-common/images/social/svg/icon-email.svg" alt="Email">
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- / Page Footer Top -->

    <!-- Page Footer Middle -->
    <div class="page-footer-middle">
        <div class="footer-container">
            <div class="row">
                <div class="block-1">

                    <!-- Footer-Links -->
                    <div class="page-footer-links ">
                        <div class="footer-links-head in">
                            <h4>My Account</h4>
                        </div>
                        <div class="footer-links collapse in">
                            <ul>
                                <li class="fc-1-1">
                                    <a href="#">Sign in</a>
                                </li>
                                <li class="fc-1-2">
                                    <a href="#">Make a payment</a>
                                </li>
                                <li class="fc-1-3">
                                    <a href="#">Spread the cost</a>
                                </li>
                                <li class="fc-1-4">
                                    <a href="#">Track my order</a>
                                </li>
                                <li class="fc-1-5">
                                    <a href="#">Quick order</a>
                                </li>
                                <li class="fc-1-6">
                                    <a href="#">Wish list</a>
                                </li>
                                <li class="fc-1-7">
                                    <a href="#">Terms &amp; conditions</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- / Footer-Links -->

                </div>
                <div class="block-2">

                    <!-- Footer-Links -->
                    <div class="page-footer-links ">
                        <div class="footer-links-head in">
                            <h4>About Us</h4>
                        </div>
                        <div class="footer-links collapse in">
                            <ul>
                                <li class="fc-2-1">
                                    <a href="#">Our history</a>
                                </li>
                                <li class="fc-2-2">
                                    <a href="#">Site map</a>
                                </li>
                                <li class="fc-2-3">
                                    <a href="#">Ethical trading</a>
                                </li>
                                <li class="fc-2-4">
                                    <a href="#">Useful information</a>
                                </li>
                                <li class="fc-2-5">
                                    <a href="#">VIP club</a>
                                </li>
                                <li class="fc-2-6">
                                    <a href="#">Media centre</a>
                                </li>
                                <li class="fc-2-7">
                                    <a href="#">Blog</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- / Footer-Links -->

                </div>
                <div class="block-3">

                    <!-- Footer-Links -->
                    <div class="page-footer-links ">
                        <div class="footer-links-head in">
                            <h4>Contact Us</h4>
                        </div>
                        <div class="footer-links collapse in">
                            <ul>
                                <li class="fc-3-1">
                                    <a href="#">Help</a>
                                </li>
                                <li class="fc-3-2">
                                    <a href="#">FAQs</a>
                                </li>
                                <li class="fc-3-3">
                                    <a href="#">How to contact us</a>
                                </li>
                                <li class="fc-3-4">
                                    <a href="#">Incentives</a>
                                </li>
                                <li class="fc-3-5">
                                    <a href="#">Feedback</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- / Footer-Links -->

                </div>
                <div class="block-4">

                    <!-- Footer-Links -->
                    <div class="page-footer-links ">
                        <div class="footer-links-head in">
                            <h4>Delivery</h4>
                        </div>
                        <div class="footer-links collapse in">
                            <ul>
                                <li class="fc-4-1">
                                    <a href="#">Delivery</a>
                                </li>
                                <li class="fc-4-2">
                                    <a href="#">Returns</a>
                                </li>
                                <li class="fc-4-3">
                                    <a href="#">Click &amp; collect</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- / Footer-Links -->

                </div>
            </div>
        </div>
    </div>
    <!-- / Page Footer Middle -->

    <!-- Page Footer Bottom -->
    <div class="page-footer-bottom">
        <div class="footer-container">
            <div class="row">
                <div class="row-height">
                    <div class="block-1">

                        <!-- Text Component -->
                        <p>
                            <small>Copyright � 2016 Simply Be.</small>
                        </p>
                        <!-- / Text Component -->

                    </div>
                    <div class="block-2">

                        <!-- Security Icon -->
                        <a href="#"><img class="secure-middle" src="http://images2.drct2u.com/hybris/common/images/payment/norton-logo.png" alt="Norton Secured central"></a>
                        <!-- Security Icon -->

                    </div>
                    <div class="block-3">

                        <!-- Credit Card Icons -->
                        <div class="cc-icons">
                            <a href="#"><img src="http://images2.drct2u.com/hybris/common/images/payment/MasterCard_Logo.svg" alt="Mastercard"></a>
                            <a href="#"><img src="http://images2.drct2u.com/hybris/common/images/payment/Maestro_logo.svg" alt="Maestro"></a>
                            <a href="#"><img src="http://images2.drct2u.com/hybris/common/images/payment/Visa_Inc._logo.svg" alt="Visa"></a>
                            <a href="#"><img src="http://images2.drct2u.com/hybris/common/images/payment/Visa_Electron.svg" alt="Visa Electron"></a>
                        </div>
                        <!-- / Credit Card Icons -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / Page Footer Bottom -->
</footer>

</div>

</div>
