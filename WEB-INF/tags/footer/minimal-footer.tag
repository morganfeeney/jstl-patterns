<footer class="page-footer-minimal">
    <div class="container-fluid">
        <div class="container-max-width">
            <div class="block-1">
                <p>Need Help? Got a Question? Give us a call on <a href="tel:08712315000">0871 231 5000</a> (Calls cost 13p per minute plus your phone company’s access charge)</p>
            </div>
            <div class="block-2">
                <a href="#"><img src="http://images2.drct2u.com/hybris/common/images/payment/norton-logo.png" alt="Norton Secured central"></a>
            </div>
        </div>
    </div>
</footer>