<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="backButton" required="false" %>

<div id="minimal-header" class="container-fluid">
	<div class="container-max-width">
		<div class="row minimal-header-row">

            <%-- Optional back button --%>

			<div class="col-xs-4 text-left">
                <c:if test="${backButton eq true}">
	                <button type="button" class="btn btn-default text-right" data-toggle="collapse" data-target="#exit-page">Back</button>
                </c:if>
			</div>

			<div class="col-xs-4">
				<div id="min-brand-logo">
					<div class="site-logo">
						<a href="#">
							<img src="http://images.drct2u.com/repo/DAL/desktop/header/02-images/SimplyBeLogo.svg" class="img-responsive" alt="Image alt text" title="Image alt text">
						</a>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
			</div>
		</div>
	</div>
</div>
