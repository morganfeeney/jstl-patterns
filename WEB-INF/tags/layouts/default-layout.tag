<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>
<%@ taglib prefix="partial" tagdir="/WEB-INF/tags/partials" %>
<%@ taglib prefix="molecule" tagdir="/WEB-INF/tags/molecules"%>
<%@ taglib prefix="organism" tagdir="/WEB-INF/tags/organisms" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/navigation"%>

<%@ attribute name="pageTitle" required="true" rtexprvalue="true" %>
<%@ attribute name="bodyClass" required="false" %>

<template:master pageTitle="${pageTitle}" bodyClass="${bodyClass}" scripts="${scripts}" templateCss="${templateCss}">
	<jsp:doBody/>
</template:master>
