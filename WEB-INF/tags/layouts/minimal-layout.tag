<%@ attribute name="pageTitle" required="true" rtexprvalue="true" %>
<%@ attribute name="bodyClass" required="false" %>

<%@ tag description="Page layout" pageEncoding="UTF-8" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/header" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/footer" %>

<template:master pageTitle="${pageTitle}" bodyClass="${bodyClass}">
	<header:minimalHeader backButton="true"/>
	<div id="main-content" class="container-fluid reverse">
		<div class="container-max-width">
			<jsp:doBody/>
		</div>
	</div>
	<footer:minimal-footer/>
    <script id="minimal-dependencies"></script>
</template:master>
