<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>
<%@ taglib prefix="partial" tagdir="/WEB-INF/tags/partials" %>
<%@ taglib prefix="molecule" tagdir="/WEB-INF/tags/molecules"%>

<%-- This is roughly what I want --%>
<c:set var="layoutCss" value="_ui/responsive/common/css/pdp-layout.css" scope="page" />

<div class="pdp-layout">
  <jsp:doBody/>
</div>
