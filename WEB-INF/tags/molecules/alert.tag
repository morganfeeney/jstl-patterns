<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="alertType" required="true" %>
<%@ attribute name="dismissable" required="false" %>

<div class="alert<c:if test='${not empty alertType}'> alert-${alertType}</c:if><c:if test='${not empty dismissable}'> alert-dismissible</c:if>">
    <c:if test='${not empty dismissable}'>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span class="icon icon-close" aria-hidden="true"></span>
        </button>
    </c:if>
    <div class="alert-inner">
        <jsp:doBody />
    </div>
</div>
