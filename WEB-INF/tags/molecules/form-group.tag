<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="hasError" required="false" %>

<div class="form-group <c:if test='${not empty hasError}'>has-error</c:if>">
    <jsp:doBody />
</div>
