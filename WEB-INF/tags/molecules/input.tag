<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="label" required="false" %>
<%@ attribute name="inputType" required="false" %>
<%@ attribute name="inputId" required="false" %>
<%@ attribute name="inputLabel" required="false" %>

<c:if test="${not empty inputLabel}">
    <label for="test-1">Test label</label>
</c:if>
<c:if test="${not empty inputType}">
    <c:choose>
        <c:when test="${inputType == 'text'}">
            <input type="text" <c:if test="${not empty inputId}"> id="${inputId}"</c:if> class="form-control" name="" value="test">
        </c:when>
    </c:choose>
</c:if>
