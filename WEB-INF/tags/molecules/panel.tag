<%--
    Panel
    ----------------------------------------------------------------------------
    Create panels with or without headings that can also be collapsible.
    ----------------------------------------------------------------------------
    To output any of the following (1,2,3) you must pass in keyword arguments.

    1. Panel body only                   panelType
    2. Panel with heading                panelType panelHeading
    3. Panel with collapsible heading    panelType panelHeading collapsable id
    ----------------------------------------------------------------------------
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="panelType" required="true" %>
<%@ attribute name="additionalClasses" required="false" %>
<%@ attribute name="panelHeading" required="false" %>
<%@ attribute name="collapsable" required="false" %>
<%@ attribute name="id" required="false" %>

<div class="panel<c:if test='${not empty panelType}'> panel-${panelType}</c:if><c:if test='${not empty additionalClasses}'> ${additionalClasses}</c:if>">
    <c:if test='${not empty panelHeading}'>
        <c:choose>
            <c:when test='${not empty collapsable and not empty panelHeading and not empty id}'>
            	<div class="panel-heading panel-heading-collapse" role="tab" id="collapse-heading-${id}">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse-content-${id}" aria-expanded="false" aria-controls="collapse-content-${id}">
                            ${panelHeading}
                        </a>
                    </h4>
                </div>
            </c:when>
            <c:otherwise>
                <div class="panel-heading">
                    <h4 class="panel-title">
                        ${panelHeading}
                    </h4>
                </div>
            </c:otherwise>
        </c:choose>
    </c:if>
    <c:choose>
        <c:when test='${not empty collapsable and not empty panelHeading and not empty id}'>
            <div id="collapse-content-${id}" class="collapse " role="tabpanel" aria-labelledby="collapse-heading-${id}">
        		<div class="panel-body">
                    <jsp:doBody/>
        		</div>
        	</div>
        </c:when>
        <c:otherwise>
            <div class="panel-body">
                <jsp:doBody/>
            </div>
        </c:otherwise>
    </c:choose>
</div>
