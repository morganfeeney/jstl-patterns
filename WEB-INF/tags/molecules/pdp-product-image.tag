<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="imageUrl" required="true" %>

<img
  class="img-responsive"
  srcset="
    https://i1.adis.ws/i/simplybe/${imageUrl}?w=330 330w,
    https://i1.adis.ws/i/simplybe/${imageUrl}?w=440 440w,
    https://i1.adis.ws/i/simplybe/${imageUrl}?w=550 550w,
    https://i1.adis.ws/i/simplybe/${imageUrl}?w=660 660w,
    https://i1.adis.ws/i/simplybe/${imageUrl}?w=880 880w,
    https://i1.adis.ws/i/simplybe/${imageUrl}?w=1100 1100w"

  src="https://i1.adis.ws/i/simplybe/${imageUrl}"

  sizes="
    (min-width: 992px) 550px,
    (min-width: 768px) 440px,
    (min-width: 590px) 330px,
    100vw"

  alt="A srcset example image">
