<div class="collapsing-side-nav">
	<h4 class="heading">Browse Your Account</h4>
	<ul class="nav nav-pills nav-stacked">
		<li>
			<a href="#one">Heading One</a>
			<ul class="nav" id="one">
				<li class="active"><a href="#">Link text</a></li>
				<li><a href="#">Link text</a></li>
				<li><a href="#">Link text</a></li>
				<li><a href="#">Link text</a></li>
			</ul>
		</li>
		<li>
			<a href="#two">Heading Two</a>
			<ul class="nav" id="two">
				<li><a href="#">Link text</a></li>
				<li><a href="#">Link text</a></li>
				<li><a href="#">Link text</a></li>
				<li><a href="#">Link text</a></li>
			</ul>
		</li>
	</ul>
</div>
