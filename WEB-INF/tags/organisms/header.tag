<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="partial" tagdir="/WEB-INF/tags/partials" %>
<%@ taglib prefix="molecule" tagdir="/WEB-INF/tags/molecules"%>

<header>
  <jsp:doBody/>
</header>
