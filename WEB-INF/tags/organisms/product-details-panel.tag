<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>
<%@ taglib prefix="partial" tagdir="/WEB-INF/tags/partials" %>
<%@ taglib prefix="molecule" tagdir="/WEB-INF/tags/molecules"%>

<%@ attribute name="exampleVar" required="true" %>

<molecule:panel panelType="default" panelHeading="Default">
  <molecule:alert alertType="${exampleVar}">
      <p>Here's the product details panel</p>
  </molecule:alert>

  <jsp:doBody/>
</molecule:panel>
