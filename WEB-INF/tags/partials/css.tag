<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="templateCss" required="false" %>
<%@ attribute name="layoutCss" required="false" %>

<%-- Scripts used on all pages --%>
<link rel="stylesheet" type="text/css" media="all" href="http://uidev.drct2u.com/uidemo/develop/themes/theme-common/css/style.css" />

<%-- Additional scripts - added by templates --%>
<c:forEach items="${templateCss}" var="stylesheet" varStatus="loop">
  <link rel="stylesheet" type="text/css" id="${loop.index}" href="${templateCss}"></script>
</c:forEach>

<%-- Additional scripts - added by layouts --%>
<c:forEach items="${layoutCss}" var="stylesheet" varStatus="loop">
  <link rel="stylesheet" type="text/css" id="${loop.index}" href="${layoutCss}"></script>
</c:forEach>
