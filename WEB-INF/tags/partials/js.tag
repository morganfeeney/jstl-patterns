<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="scripts" required="false" %>

<%-- Scripts used on all pages --%>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<%-- Additional scripts - added by layouts --%>
<c:forEach items="${scripts}" var="script" varStatus="loop">
    <script id="${loop.index}" src="${script}"></script>
</c:forEach>
