<%@ attribute name="pageTitle" required="true" rtexprvalue="true" %>
<%@ attribute name="bodyClass" required="false" %>
<%@ attribute name="scripts" required="false" %>
<%@ attribute name="templateCss" required="false" %>
<%@ attribute name="layoutCss" required="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>
<%@ taglib prefix="partial" tagdir="/WEB-INF/tags/partials" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/header" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/footer" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/navigation"%>


<html>
	<head>
		<title>${pageTitle}</title>

        <%--
             CSS - default CSS used on all pages, with ability to pass in
             additional CSS files where needed
         --%>

        <partial:css templateCss="${templateCss}" layoutCss="${layoutCss}"/>

        <%-- Feature detection --%>
		<script src="_ui/responsive/common/js/vendor/modernizr.js"></script>

        <%-- Dev only scripts --%>
		<script src="http://localhost:35729/livereload.js"></script>

        <%-- Dependency scripts --%>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	</head>

	<body class="${bodyClass}">
		<div class="site-wrapper">
           <jsp:doBody/>
  		</div>
	</body>

    <%--
         JS - default JS used on all pages, with ability to pass in additional
         scripts where needed
     --%>

    <partial:js scripts="${scripts}"/>

</html>
