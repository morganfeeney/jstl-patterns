<%@ attribute name="pageTitle" required="true" rtexprvalue="true" %>
<%@ attribute name="bodyClass" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ tag description="Page layout" pageEncoding="UTF-8" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="organism" tagdir="/WEB-INF/tags/organisms" %>

<%@ attribute name="templateCss" required="false" %>
<%@ attribute name="layoutCss" required="false" %>

<%-- Layout specific scripts --%>
<c:set var="scripts" value="script-one.js,script-two.js,script-three.js" />

<template:master pageTitle="${pageTitle}" bodyClass="${bodyClass}" scripts="${scripts}" templateCss="${templateCss}" layoutCss="${layoutCss}">
  <div id="main-content" class="container-fluid">
    <div class="container-max-width">

      <organism:header>
        <p>Header content</p>
      </organism:header>

      <jsp:doBody/>

      <organism:footer/>

    </div>
  </div>
</template:master>
