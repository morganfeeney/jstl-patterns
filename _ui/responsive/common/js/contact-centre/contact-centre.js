!function() {
    /**
     * Author: Simon Mo
     * Email: simon.mo@jdwilliams.co.uk
     * Description: contactCentre library object that holds all contact centre related functions
     **/

    var contactCentre = {
        version: "1.0.0",
        url: null
    };

    /**
     * Author: Simon Mo
     * Email: simon.mo@jdwilliams.co.uk
     * Description: contactCentre function to check if a string contains another string
     **/
    contactCentre.contains = function(string, substring){
      return string.indexOf(substring) > -1
    }

    /**
     * Author: Simon Mo
     * Email: simon.mo@jdwilliams.co.uk
     * Description: Set Url specific parameters
     **/

    contactCentre.setParams = function(){
      switch(window.location.host){
        case "uidev.drct2u.com":
          if(contactCentre.contains(window.location.pathname,"feature")){
            contactCentre.url = window.location.origin + "/uidemo/feature/contact-centre";
          } else if(contactCentre.contains(window.location.pathname,"master")) {
            contactCentre.url = window.location.origin + "/uidemo/master";
          }
        break;
        default:
          contactCentre.url = window.location.origin;
        break;
      }
    }

    /**
     * Author: Simon Mo
     * Email: simon.mo@jdwilliams.co.uk
     * Description: Navigates back one page in browser history
     **/

    contactCentre.goBack = function(){

      // Navigate back one page
      window.history.back(-1);

      // Notify JS engine that function has finished running
      return;

    };

    /**
     * Author: Simon Mo
     * Email: simon.mo@jdwilliams.co.uk
     * Description: Navigates forward in browser history
     **/

    contactCentre.goForwards = function(){

      // Navigate forward one page
      window.history.forward();

      // Notify JS engine that function has finished running
      return;

    };

    /**
     * Author: Simon Mo
     * Email: simon.mo@jdwilliams.co.uk
     * Description: Handles keypresses and highlights mega-menu when alt key is pressed
     **/

    contactCentre.keyHandler = function(e) {

        // Check if alt key has been pressed
        if(e.altKey === true) {
          // User has pressed alt key so add class to highlight div.mega-menu
          $('.mega-menu').addClass('active');
        }

        // Check if altkey and another key has been pressed
        if((e.altKey === true) && (e.keyCode !== 18)){
            // Pass key that has been pressed into switch
            switch(e.keyCode) {

                // User has pressed left key
                case 37:
                  // Navigate back one page
                  contactCentre.goBack();
                break;

                // User has pressed right key
                case 39:
                  // Navigate forward one page
                  contactCentre.goForwards();
                break;

                // User has pressed a key besides alt, left or right key
                default:
                  // Convert keyCode into it's key value
                  var shortcutCode = String.fromCharCode(e.keyCode).toLowerCase();
                  // Process shortcut
                  contactCentre.processShortcut(shortcutCode);
                break;

            }

        }

        // Notify JS engine that function has finished running
        return;

    };

    /**
     * Author: Simon Mo
     * Email: simon.mo@jdwilliams.co.uk
     * Description: Processes shortcut to perform it's defined action or return an error message
     **/

    contactCentre.processShortcut = function(shortcutCode){

      // Select element with data-shortcut attribute that matches shortcutCode
      var navItem = $('.nav-item[data-shortcut="' + shortcutCode + '"]');

      // Check if element selected has a data-link attribute and determine if this attribute is NOT empty
      if((typeof navItem.data('link') !== "undefined") && (navItem.data('link') !== "")){

        // Element has a data-link attribute and is NOT empty so add active class to element
        navItem.addClass('active');

        // Navigate to link
        window.location.href = contactCentre.url + navItem.data('link');

      } else if(typeof navItem.data('link') !== "undefined"){

        // Element has a data-link attribute and is empty so return console log error message
        console.log('The shortcut link for this shortcut has not been setup, please check the data-link attribute on the following div. <div class="section nav-item" data-shortcut="' + shortcutCode + '">');

      } else {

        // Element does not have a data-link attribute so return console log error message
        console.log("This shortcut does not exist");

      }

      // Notify JS engine that function has finished running
      return;

    };

    if (typeof define === "function" && define.amd) this.contactCentre = contactCentre, define(contactCentre); else if (typeof module === "object" && module.exports) module.exports = contactCentre; else this.contactCentre = contactCentre;
}();



$(document).ready(function() {

    // Set Url specific parameters
    contactCentre.setParams();

    // Select all tooltip enabled elements
    var tooltips = $('[data-toggle="tooltip"]');

    // Setup Bootstrap 3 tooltips on accounts page
    tooltips.tooltip();

    // Setup hover event listener for adding exclaimation mark to element on hover over for all tooltips
    tooltips.hover(function(){
      $(this).addClass('alert alert-info');
    },function(){
      $(this).removeClass('alert alert-info');
    });

    /**
    * Author: Simon Mo
    * Email: simon.mo@jdwilliams.co.uk
    * Description: Event listeners
    **/
    // Event Listener for back button
    $(".nav-back").click(function(){
      contactCentre.goBack();
    });

    // Event Listener for forward button
    $(".nav-forwards").click(function(){
      contactCentre.goForwards();
    });

    // Event Listener for keyboard shortcuts
    $(window).on('keydown',function(e){
      contactCentre.keyHandler(e);
    });

    // Event Listener for end of keyboard shortcut
    $(window).on('keyup',function(e){
      if(e.altKey === false){
        $('.mega-menu').removeClass('active');
      }
    });

    // Event Listener for mega-menu hover, first function is over, second function is out
    $('.mega-menu').hover(function(){

      // Add active class to mega-menu
      $(this).addClass('active');

    },function(){

      // Remove active class to mega-menu
      $(this).removeClass('active');

    });

    // Event Listener for mega-menu nav item
    $('.nav-item').click(function(){

      // Get shortcut key
      var shortcutCode = $(this).data('shortcut');

      // Process shortcut
      contactCentre.processShortcut(shortcutCode);

    });

    // Event listener for search button
    $('.customer-search-button').click(function(){

      // Show search results on click
      $('.contact-centre-search-results').removeClass('hidden');

    });

    // Event listener for search button
    $('.customer-search-button').click(function(){

      // Show search results on click
      $('.contact-centre-search-results').removeClass('hidden');

    });

    // Event listener for view account button on /templates/contact-centre/contact-centre-search-customer.html
    $('.btn-view-account').click(function(){

      // Show search results on click
      window.location.href = contactCentre.url + "/templates/contact-centre/contact-centre-search-customer-details.html";

    });

    // Event listener for create account button on /templates/contact-centre/contact-centre-search-customer.html
    $('.btn-create-account').click(function(e){
      e.preventDefault();
      // Show search results on click
      window.location.href = contactCentre.url + "/templates/contact-centre/contact-centre-retail-account-creation.html";

    });

    // Event listener for continue button on /templates/contact-centre/contact-centre-retail-account-creation.html
    $('.btn-continue').click(function(e){
      e.preventDefault();
      // Show search results on click
      window.location.href = contactCentre.url + "/templates/contact-centre/contact-centre-additional-details-capture.html";

    });

    // Request Catalog Button following

    $('.req-cat-btm-btn').find('button').click(function(e){
      e.preventDefault();

      window.location.href = contactCentre.url + "/templates/contact-centre/contact-centre-catalogue-request-confirmation.html";
    });

    // Loop through all buttons and add a click event listener to goBack in window history for every Back Button.
    $('.btn').each(function(data,id){

      // Does button have the string "Back"
      if($(this).html() == "Back"){

        // Button has string "Back"
        $(this).click(function(){

          // Go back one page when clicked
          contactCentre.goBack();

        });

      }

    });

    // Add event listener to contact preferences icon's parent container so click zone is larger
    $('.contact-icon').parent().click(function(){
      // Once clicked get the icon's details

      // Get Icon
      var icon = $(this).find('.contact-icon'),

          // Get attribute data-brand which is the brand they have clicked on
          brand = icon.data('brand'),

          // Get attribute data-type which is contact preference i.e e-mail
          type = icon.data('type'),

          // Get both icons from ASM bar and also page if they are on a page where the contact preference grid also exists
          iconPair = $('.contact-icon[data-brand="' + brand + '"][data-type="' + type + '"]');

      switch(true){

        // Icon is currently green
        case icon.hasClass('text-green'):

          // Remove text-green class and add text-red class
          iconPair.removeClass('text-green').addClass('text-red');

        break;

        // Icon is currently red
        case icon.hasClass('text-red'):
          // Remove text-red class and add text-green class
          iconPair.removeClass('text-red').addClass('text-green');

        break;
      }

    });

    // Account Summary
    function checkThirdPartyText(){
      if (($('#account-thirdparty-text-static').children('p').html() == "") || $('#account-thirdparty-text-static').children('p').html() == undefined){
        $('#account-thirdparty-text-static').children('p').html("<p style=\"color:#adadad\">No third party information</p>")
      }
    }

    checkThirdPartyText();

    $('#account-thirdparty-button-edit').click(function(){
      $('#account-thirdparty-text-static').toggleClass('hidden');
      $('#account-thirdparty-text-edit').toggleClass('hidden');
    });

    $('#account-thirdparty-button-save').click(function(){
      $('#account-thirdparty-text-static').toggleClass('hidden');
      $('#account-thirdparty-text-edit').toggleClass('hidden');
      $('#account-thirdparty-text-static').children('p').html($('#account-thirdparty-textarea').val());
      checkThirdPartyText()
    });

    // Quick order


    $('.btn-quick-order').each( function() {

      $(this).click( function() {
          if($(this).parent().parent().parent().find('#personaliseInput').val() == ""){
            $('#PersonalisedModal').modal('show');
          }
      });

    });

    $('.btn-end-call').click(function(){
      $('#modal-end-call').modal('show');
    });

    $('.btn-log-out').click(function(){
      $('#modal-log-out').modal('show');
    });

    if($('form').hasClass('form-add-address')){

        $('.add-address-actions-btm .btn').click(function(){
          console.log('unset');
          window['failed'] = false;
        });

        var formInputs = $('.form-add-address').find('input');

        formInputs.one('keydown',function(){

          if(window['inputStarted'] === true){
            return false;
          }

          window['inputStarted'] = true;
          window['failed'] = true;

          console.log('keydown');

          $(window).bind('beforeunload', function(e){

            e.preventDefault();

            if(window['failed'] == true){
                return 'Please capture an address';
            } else {
              console.log('passed');
            }

          });

      });

    };

    $('.add-contact-number-actions-btm').one('click',function(){

      $('.contact-numbers').removeClass('hidden');
      $(this).find('button').text('Save');

      $('.add-contact-number-actions-btm').click(function(e){

          var input = $('.primary-number').val();
            console.log(input);

          if((input.length >= 9) && (input.length <= 15)){
            return
          } else {
            $('.notify').removeClass('hide');
            e.preventDefault();
          };

	    });

    });

});
