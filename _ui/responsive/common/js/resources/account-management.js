$(function(){
	// Account management delete button will show the modal
	$('.account-management-delete-btn').on('click', function (event) {
		var id = $(event.target).closest('.account-item').data('id');
		$('#deleteConfModal').data('id', id).modal('show');
	});

	// Modal confirm delete button >
	$('.btn-delete-card').click(function() {
		
		// Handle the deletion of the item based on the data-id="" attribute
		var id = $('#deleteConfModal').data('id');
		$('[data-id="'+id+'"]').remove();
		$('#deleteConfModal').modal('hide');

		//Check to see if the <div class="account-management-items"> contains child elements, if it does, then it will show 2 buttons add-(card || address) buttons, one above and one below the elements.
		//If all child elements have been deleted from the <div class="account-management-items">, only one button will show, the top one.
		if ($('.account-management-items').children().length == 0) {
			$('.actions-btm').hide();
		}
		else {
			$('.actions-btm').show();
		}
	});
});

$('.edit-card-address-btn').click(function(){  
	$('.edit-card-address').show();
	$('.card-address').hide();
});
$('.addEditAddress-btn').click(function(){  
	$('.address-toggle').collapse('toggle');
});