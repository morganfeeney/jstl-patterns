// // Setup account sidebar for different breakpoints
// function setupAccountNavigation(){
// 	var linkGroupHeadings = $('.panel-title-wrapper, #account-nav-title');
// 	var linkGroups = $('.nav-link-group, #account-nav');
//
// 	if (BootstrapFunctions.isBreakpoint(['xs', 'phablet-p'])){
// 		linkGroupHeadings.each(function(){
// 			$(this).addClass('collapsed').attr('aria-expanded', 'false');
// 			if ($(this).hasClass('sidebar-title')){
// 				$(this).find('.burger').removeClass('active');
// 			}
// 		});
// 		linkGroups.each(function(){
// 			$(this).removeClass('in').attr('aria-expanded', 'false').css('height', '0px');
// 		});
// 	} else {
// 		linkGroupHeadings.each(function(){
// 			$(this).removeClass('collapsed').attr('aria-expanded', 'true');
// 		});
// 		linkGroups.each(function(){
// 			$(this).addClass('in').attr('aria-expanded', 'true').removeAttr('style');
// 		});
// 	}
// }
//
// $(function(){
// 	// Collapse the sidebar if on xs
// 	if (BootstrapFunctions.isBreakpoint(['xs', 'phablet-p'])){
// 		$('#account-nav').removeClass('in');
// 	}
// 	// Toggle the burger icon
// 	$(".account-mobile-sidebar-heading .panel-title-wrapper").bind( "click", function() {
// 		$(this).find('.burger').toggleClass("active");
// 		});
// 	// Stop collapse from firing unless on xs
// 	$(".panel-link-group").bind('show.bs.collapse', function(){
// 		if (!BootstrapFunctions.isBreakpoint(['xs', 'phablet-p'])) return false;
// 	});
// 	$(".panel-link-group").bind('hide.bs.collapse', function(){
// 		if (!BootstrapFunctions.isBreakpoint(['xs', 'phablet-p'])) return false;
// 	});
// 	// Toggle sidebar link visibility when crossing breakpoints
// 	$('body').bind('breakpointHit', setupAccountNavigation);
// });

$(function(){
	$('.collapsing-side-nav > ul > li > a, .collapsing-side-nav .heading').on('click', function(event){
		event.preventDefault();
		$(this).parent().toggleClass('active');
	});
});