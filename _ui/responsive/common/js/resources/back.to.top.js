jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
	//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
	offset_opacity = 1200,
	//duration of the top scrolling animation (in ms)
	// scroll_top_duration = 700,
	//grab the "back to top" link
	$back_to_top = $('.cd-top');

	// other scrolling options (general scroll / button mouse leave)
	detectScrollShow = function (){
		var scrollTimeDone;
		if (scrollTimeDone) clearTimeout(scrollTimeDone);
		$(window).scroll(function(){
			( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible');
			if( $(this).scrollTop() > offset_opacity ) {
				$back_to_top.addClass('cd-is-visible');
			}
		});
		$('.cd-top').mouseleave(function() {
			scrollTimeDone = setTimeout(function(){
		        $back_to_top.removeClass('cd-is-visible');
		    }, 0);
		});
	};
	detectScrollShow();

	// debounce function
	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			clearTimeout(timeout);
			timeout = setTimeout(function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			}, wait);
			if (immediate && !timeout) func.apply(context, args);
		};
	};

	// debounce scroll detection
	detectScrollDelay = function (){
		// detect the end of the scroll
		var delayedScroll = debounce(function() {
			//if($back_to_top.is(".cd-top:hover")) {
			if($('.cd-top:hover').length > 0) {
		       $back_to_top.addClass('cd-is-visible');
		    }
		    else {
		       $back_to_top.removeClass('cd-is-visible');
		    }
		}, 4000);

		$(window).on('scroll', function(){
			delayedScroll();
		})
	};
	detectScrollDelay();

	//smooth scroll to top
	// $back_to_top.on('click', function(e){
	// 	$('body,html').animate({
	// 		scrollTop: (0,0) ,
	// 	 	}, scroll_top_duration
	// 	);
	// 	e.preventDefault();
	// });

});