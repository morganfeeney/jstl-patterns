$(function(){

	// show / hide the breadcrumbs based on the last breakpoint
	function toggleBreadcrumbVisibility(){
		if (BootstrapFunctions.isBreakpoint('xs')){
			$('#breadcrumb').addClass('hide');
			$('#breadcrumb-mobile').removeClass('hide');
		}
		else {
			$('#breadcrumb').removeClass('hide');
			$('#breadcrumb-mobile').addClass('hide');
		}
	}
	toggleBreadcrumbVisibility();
	$('body').bind('breakpointHit', toggleBreadcrumbVisibility);
});