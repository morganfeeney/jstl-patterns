(function($) {
	var CheckoutFunctions = function ($){
		var self = this;
		var preSelect;

		self.tickboxGroupToggle = function() {
			$('.radio-group label, .checkbox-group label').on('click', function() {
				var target = $(event.target);
				var $target_parent = $(target).closest('.custom-rc, .custom-checkbox');
				var $target_input = target.prev('input');
				var $currentOption = $('.custom-rc input:checked');

				// If radio button is already 'checked' or 'disabled', quit function.
				if ( $target_input.attr('type') == 'radio' && ($target_input.prop('checked') || $target_input.prop('disabled')) ) {
					return false;
				}

				// If delivery address has collection option, toggle delivery speed options
				if ( $target_input.attr('name') == 'DEL-OPTION' ) {
					self.updateDeliverySpeedOptions($target_input);
				}

				// Initialise first-order prompt if exists on page, but not set.
				if ( $('[data-modal="first-order-prompt"]').length > 0 ) {
					if ( sessionStorage.getItem('first-order-prompt') == null && preSelect == 'false') {
						sessionStorage.setItem('first-order-prompt', 'true');
					}
				}

				// If first-order is the current selection when a payment method is clicked -
				// check html storage for flag before prompting the first-order mpdal.
				// Will only prompt once until the session ends.
				if ( $('[data-modal="first-order-prompt"]').length > 0 && $(target).data('toggle') == 'false') {
					if ( $($currentOption).data('modal') == 'first-order-prompt' ) {
						if ( sessionStorage.getItem('first-order-prompt') == 'true') {
							$('#modal-first-order-prompt').modal();
							checkoutPaymentTargetOption = $target_parent;
							sessionStorage.setItem('first-order-prompt', false);
							return false;
						}
					}
				}

				// Change the Place Order CTA label depending on the payment selection
				if ( $($target_parent).data('payment') ) {
					self.changeCheckoutLabel($target_parent, $($target_parent).data('payment'));
				}

				// If the parent tickbox is 'checked' we don't want to collapse or clear anything
				if ( !$($target_parent).closest('.custom-rc', 'custon-checkbox').children('input').prop('checked') ) {
					self.clearAllCheckboxes($target_parent);
					self.collapseAll($target_parent);
				}

				self.openCollapsable($target_parent);
				// preSelect is a temp solution to prevent 'first-order-promt' modal triggering when 'preSelectPaymentOption' function is run. In production the first payment option should be set using html, not js.
				preSelect = 'false';
			});
		};

		self.nominatedSelectFunction = function() {
			$('.time-slot input').on('click', function() {
				var nom_date = $('.nominated-list input[name="NOM-DELIVERY"]:checked + label').text();
				var nom_time = $('.time-slot input[name="NOM-TIME"]:checked').val();
				var separator = nom_time == "" ? "" : " - ";
				var nominated_selection = nom_date + separator + nom_time;
				// update nominated day label
				$('#nominated_day_display').html(nominated_selection);
				// close and clear
				clearAll('#delivery-nominated-list');
				// show 'change' button
				$('#change-nominated-btn').fadeIn().on('click', function() {
					$( $(this).data('target') ).collapse('show');
				});
				// Show the info message regarding 'nominated guarantee' if AM pr PM selected.
				if (nom_time) {
					$('#nominated-info-message').fadeIn();
				} else {
					$('#nominated-info-message').slideUp();
				}
			});
		};

		self.clearAll = function(elem) {
			$(elem).find('input').each( function() {
				this.checked = false;
			});
			$(elem).collapse('hide');
			$(elem).find('.collapse').collapse('hide');
		};

		self.bnplMoreInfo = function() {
			$('.bnpl-pay-options input').change( function() {
				$('#' + $(this).attr('name')).collapse('show');
			});
		};

		self.preSelectPaymentOption = function() {
			var preSelect = true;
			var $firstOption = $('.checkbox-payment-method > .custom-rc:not(.disabled)').first();
			self.openCollapsable($firstOption);
			$($firstOption).children('label').trigger('click');
		};

		self.updateDeliverySpeedOptions = function(target) {
			switch(target.attr('id')) {
			    case 'DELIVERY-OPTION':
					$('.delivery-speed-list > .checkbox-group > div').slice(1,3).slideDown('fast');
			    	break;
			    case 'COLLECTION-OPTION':
					$('.delivery-speed-list > .checkbox-group > div').slice(1,3).slideUp('fast');
					var $first = $('.delivery-speed-list > .checkbox-group > div:first-child').children('input');
					self.collapseAll($first);
					self.clearAllCheckboxes($first);
					$first.prop('checked', 'true');
			    	break;
			}
		};

		self.changeCheckoutLabel = function(elem, label) {
			// Change the Place Order CTA label depending on the payment selection
			var text = ( label == "account") ? 'Place Order & Pay on Account' : 'Place Order & Pay Now';
			$('.checkout-continue-btn button').text(text);
		};

		return {
			clearAll: clearAll,
			bnplMoreInfo: bnplMoreInfo,
			tickboxGroupToggle: tickboxGroupToggle,
			preSelectPaymentOption: preSelectPaymentOption,
			nominatedSelectFunction: nominatedSelectFunction,
		}
	};
	// Setup the global object and run init on document ready
	$(function(){
		window.CheckoutFunctions = CheckoutFunctions(jQuery);
		window.CheckoutFunctions.bnplMoreInfo();
		window.CheckoutFunctions.tickboxGroupToggle();
		window.CheckoutFunctions.preSelectPaymentOption();
		window.CheckoutFunctions.nominatedSelectFunction();
	});
})(jQuery);

function keepFirstOrderPrompt() {
	sessionStorage.setItem('first-order-prompt', 'true');
}
function removeFirstOrderDiscount() {
	clearAllCheckboxes('[data-modal="first-order-prompt"]');
	$('#modal-first-order-prompt').modal('hide');
	$(checkoutPaymentTargetOption).children('input + label').trigger('click');
	$('[data-modal="first-order-prompt"]').removeData('modal');
}
