(function($) {
	var GoogleMap = function ($){
		var self = this;
		var map;
		var myLag;
		var mapCenter;
		self.initMap = function(x, y, title, id) {
			myLag = {lat: parseFloat(x), lng: parseFloat(y)}
			map = new google.maps.Map($('#'+ id +' .map-panel')[0], {
				navigationControl: false,
				disableDefaultUI: true,
				scaleControl: false,
				scrollwheel: false,
				draggable: false,
				center: myLag,
				zoom: 15,
			});
			var marker = new google.maps.Marker({
				position: myLag,
				map: map,
				title: title,
			});
			mapCenter = map.getCenter(myLag);
			google.maps.event.addDomListener(window, "resize", function() {
				google.maps.event.trigger(map, "resize");
				map.setCenter(mapCenter); 
			});
		};
		return {
			initMap: initMap,
		}
	};
	$(function(){ window.BootstrapFunctions.GoogleMap = GoogleMap($); });

	// Click to collect - Did you mean - Search Results
	$(".postcode-search-results #location-0").on("click", function () {
		$('.collection-location-00').removeClass('hide');
		$('.collection-location-00').find('.collapse, .in').removeClass('in');
		$('.collection-location-01').addClass('hide');
	});
	$(".postcode-search-results #location-1").on("click", function () {
		$('.collection-location-00').addClass('hide');
		$('.collection-location-01').removeClass('hide');
		$('.collection-location-01').find('.collapse, .in').removeClass('in');
	});
	// Click to collect - Did you mean - Search Results / END
})(jQuery);