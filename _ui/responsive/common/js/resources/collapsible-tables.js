$(function(){
	var collapsibleTables = $('.collapsible-table');
	collapsibleTables.each(function (){
		collapsibleTable($(this));
	});
});

// New stuffs
function collapsibleTable($table){
	var table_breakpoint = $table.attr('data-table-break');
	table_breakpoint = table_breakpoint.replace(/ /g, "");
	table_breakpoint_array = table_breakpoint.split(",");

	var customTableHeadings = $table.find('.custom-table-expand a')
	var customTableSection = $table.find('.custom-table-expand-content');

	function checkBreak () {	
	 	if (BootstrapFunctions.isBreakpoint(table_breakpoint_array)){
			customTableHeadings.each(function(){
				$(this).addClass('collapsed').attr('aria-expanded', 'false');				
			});
			customTableSection.each(function(){
				$(this).removeClass('in').attr('aria-expanded', 'false').css('height', '0px');
			});
		} else {
			customTableHeadings.each(function(){
				$(this).removeClass('collapsed').attr('aria-expanded', 'true');
			});
			customTableSection.each(function(){
				$(this).addClass('in').attr('aria-expanded', 'true').removeAttr('style');
			});
		}
	}
	$('body').bind('breakpointHit', checkBreak);
	checkBreak();
}
