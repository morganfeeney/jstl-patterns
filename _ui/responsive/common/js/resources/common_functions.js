var checkoutPaymentTargetOption;

(function($) {
	jQuery.fn.clickToggle = function(a,b) {
	  var ab=[b,a];
	  function cb(){ ab[this._tog^=1].call(this); }
	  return this.on("click", cb);
	};
	var CommonFunctions = function ($){
		var self = this;
		var _dayselect, _timeselect = false;

		self.togglePassword = function() {
			try {
				$('label[for="show-password"]').bind('click touch', function() {
					var $target = $(this).closest('.form-group').find('[data-toggle="password"]');
					if ($target.attr('type') == "password") {
						$target[0].setAttribute("type", "text");
					} else {
						$target[0].setAttribute("type", "password");
					}
				});
			}
			catch(err) {
			}
		};

		// Pass in tickbox parent (rc, checkbox) and will expand any nested
		// container associated.
		self.openCollapsable = function(elem) {
			if ( $(elem).children('[data-target]').length > 0 ) {
				 $($(elem).children('[data-target]').data('target')).collapse('show');
				 $(elem).children('label').attr('aria-expanded', true);
			}
		};

		// Collapse all open accordions of the group the passed in 'elem' is part of.
		self.collapseAll = function(elem) {
	      $(elem).closest('.radio-group, .checkbox-group').find('.in').collapse('hide');
		  $(elem).closest('.radio-group, .checkbox-group').find('label').attr('aria-expanded', false);
	  	};

		// Find nearest checkbox-group and clear all child checkboxes except the one just clicked.
		self.clearAllCheckboxes = function(elem) {
			$(elem).parent('.radio-group').find('input').each( function() {
				if ( !$(this).is($(elem).find('input')) ) {
					this.checked = false;
				}
      		});
		};

		self.toggleCollapsable = function() {
			$('button[data-toggle="collapse"]').on('click', function() {
				$(this).closest('.panel').find('button[data-toggle="collapse"]').not(this).collapse('hide');
			});
		};

		// self.scrollTo = function(id, offset) {
		// 	offset = (typeof offset === 'undefined') ? 0 : offset;
		// 	$('html,body').animate({scrollTop: $(id).offset().top + offset},500);
		// };

		return {
			// scrollTo: scrollTo,
			togglePassword: togglePassword,
			openCollapsable: openCollapsable,
			toggleCollapsable: toggleCollapsable
		}
	};
	// Setup the global object and run init on document ready
	$(function(){
		window.CommonFunctions = CommonFunctions(jQuery);
		window.CommonFunctions.togglePassword();
		window.CommonFunctions.openCollapsable();
		window.CommonFunctions.toggleCollapsable();
	});
})(jQuery);
