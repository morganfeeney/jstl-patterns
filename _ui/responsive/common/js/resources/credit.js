$(function(){

  // ADD THE BG-SUCCESS CLASS TO PANEL WHEN CHECKBOX IS CHECKED
  $('#creditAppSignature').change(function(){
    if($(this).prop("checked")){
      $('#credit-app-submit').removeAttr('disabled');
      $('.credit-app-signature-panel').addClass('bg-success');
    }
    else {
      $('#credit-app-submit').prop('disabled', true);
      $('.credit-app-signature-panel').removeClass('bg-success');
    };
  });

  // CLEAR CURRENT DROPDOWN WHEN ADDING NEW ADDRESS
	$('.show-hide-add-new-address-btn').click(function(){
		var dataClear = $(this).attr('data-clear');
		$('.address-select' + '#' + dataClear).val(0);
	});

	// HIDE THE NEXT ADDRESS FORM WHEN DROPDOWN STATE CHANGES
	$('.address-select').change(function(){
		var nextForm = $(this).parents('.address-select-container').next('.show-hide-address-form-container');
		$(nextForm).removeClass('collapse in').addClass('collapse').attr('aria-expanded', 'false');
	});

  // TAB TO THE NEXT INPUT IN SSN WHEN MAXLENGTH IS REACHED
  $('.creditSSN input').keyup(function(){
    if(this.value.length == this.maxLength) {
      $(this).parent('.creditSSN').next('.creditSSN').find('input').focus();
    }
  });

});
