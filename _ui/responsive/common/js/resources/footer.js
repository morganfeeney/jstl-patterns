function setupFooter(){
	if (BootstrapFunctions.isBreakpoint(['xs', 'phablet-p', 'phablet-l'])){
		$('.footer-links, .footer-links-head').removeClass('in');
	} else {
		$('.footer-links, .footer-links-head').addClass('in');
	}
	$(".footer-links-head").off('click').on('click', function() {
		if (BootstrapFunctions.isBreakpoint(['xs', 'phablet-p', 'phablet-l'])){
			$(this).toggleClass("in");
			$(this).next('.footer-links').collapse('toggle');
			$('html, body').animate({
				scrollTop: $(this).offset().top
			}, 500);
		}
	});
}


$(function(){
	setupFooter();
	// Toggle sidebar link visibility when crossing breakpoints
	$('body').bind('breakpointHit', setupFooter);
});
