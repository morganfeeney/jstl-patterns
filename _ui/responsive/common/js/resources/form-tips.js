(function(jQuery) {
	
	var FormTips = function ($){
		
		// Check if the current page has any form tips - if it does do some initial setup and then add events
		if ($('.form-tip').length === 0) return false;

		var self = this;
		self.configs = {
			'hasLoaded' : false,
			'currentFormat' : null
		};

		// Returns the current format - Mobile / Desktop
		self.getFormat = function (){
			if (BootstrapFunctions.isBreakpoint(['xs', 'phablet-p', 'phablet-l'])){
				return "Mobile";
			} else {
				return "Desktop";
			}
		};	

		// Positions form tips above or to the right of inputs
		self.positionTips = function (){
			// If this isn't initial doc load
			if (self.configs.hasLoaded === true) {
				// Check if the format needs updating - return if not
				if (self.configs.currentFormat === self.getFormat()) return
				// else update the format
				self.configs.currentFormat = self.getFormat();				
			}

			// If mobile 
			if (self.configs.hasLoaded === false && self.getFormat() == "Mobile" || self.configs.currentFormat == "Mobile"){
				// Get all the (Desktop) form tips
				$('[data-tip]').each(function(){
					// Get the data-tip attribute - used to link the desktop tips to a form group
					var formGroup = $(this).attr('data-tip');
					// Detatch the Desktop tip from its container and add it to its corresponding form group
					$(this).find('.form-tip').detach().insertAfter($('[data-field="' + formGroup + '"]').find('label'));
				});
			} else {
				// Get all the (Mobile) form tips
				$('[data-field]').each(function(){
					// Get the tips desktop container
					var tipDesktopContainer = $(this).attr('data-field');
					// Detach the Mobile tip from the form group and add it to its corresponding desktop container
					$(this).find('.form-tip').detach().appendTo($('[data-tip="' + tipDesktopContainer + '"]'));
				});
			}			
		};
		self.positionTips();
		self.configs.hasLoaded = true;
		self.configs.currentFormat = self.getFormat();

		// Check all of the form-tips to see if any need hiding
		if ($('.form-tip.hide').length > 0){
			var hiddenTips = $('.form-tip.hide');			
			hiddenTips.each(function(){
				var tip = $(this);
				var tipIcon = tip.find('.tipicon');
				
				// Get the input for the tip
				if (self.configs.currentFormat == "Mobile"){
					var inputField = tip.parent().find('.form-control');					
				} else {
					var inputField = $('[data-field="' + tip.parent().attr('data-tip') + '"] .form-control');
				}

				// Bind the focus / blur and tip icon events
				inputField.bind('focus', function(){
					tip.removeClass('hide').addClass('fade');
					setTimeout(function(){
						tip.addClass('in');
					},0);
				});
				$(inputField).bind('blur', function(){
					tip.addClass('hide').removeClass('fade in');
				});
				$(tipIcon).bind('click', function(){
					inputField.focus();
				});					
			});
		}		

		// Expose methods / properties
		return {
			positionTips: self.positionTips
		};
	};

	// Setup the global object on document ready
	$(function(){ 
		window.FormTips = new FormTips(jQuery); 
		if (window.FormTips !== false){
			$('body').bind('breakpointHit', window.FormTips.positionTips);	
		}
	});

})(jQuery);