$(function(){

    function init (){
        $('.main-navigation .nav li[class^="sub-"] > a').on('touchstart', function(event){
            handleTouchStart.call(this, event);
        });
        $('.main-navigation .nav li[class^="sub-"] > a').on('touchend', function(){
            handleTouchEnd.call(this, event);
        });
        $('.mobile-search-toggle').on('click', function(){
            handleMobileSearchToggleClick.call(this);
        });
        $('.btn-header-navicon').on('click', function(){
            handleSideNavClick.call(this);
        });
        $('.mobile-drop-toggle').on('click', function(){
            handleSubNavClick.call(this);
        });
        $('.main-navigation li[class^=sub-]').on('mouseover', function (){
            handleMouseOver.call(this);
        });
        $('.mobile-overlay, .mobile-overlay-close').on('click', function(){
            handleOverlayClick.call(this);
        });
        $('.site-header .dropdown-item').on('show.bs.dropdown', function(){
            offsetMenuDropdown.call(this);
        });
    }

    function unbindEvents(){
        $('.main-navigation .nav li[class^="sub-"] > a').off('touchstart');
        $('.main-navigation .nav li[class^="sub-"] > a').off('touchend');
        $('.mobile-search-toggle, .btn-header-navicon, .mobile-drop-toggle, .mobile-overlay, .mobile-overlay-close').off('click');
        $('.main-navigation li[class^=sub-]').off('mouseover');
        $('.site-header .dropdown-item').off('show.bs.dropdown');
    }

    function clearMegaMenuCloseBtnStyling (){
        $('.mobile-overlay-close').removeAttr('style');
    }

    function closeMenu (){
        $('html').removeClass('mobile-nav-open');
        // Close any mega menu $dropdowns
        $('.open-sub-menu').removeClass('open-sub-menu');
        clearMegaMenuCloseBtnStyling();
    }

    function openMobileSubNav ($listItem){
        $listItem.siblings('.open-mobile-sub').removeClass('open-mobile-sub').find('.in').removeClass('in');
        $listItem.toggleClass('open-mobile-sub').find('.sub').toggleClass('in');
    }

    function positionMegaMenu ($dropdown){
        // Mega menu only shows on MD and LG
        var container_width = $('.main-navigation .nav').width();
        var closeBtnWidth = 0;

        // Width work around to account for borders which position() doesn't count
        var dropdown_width = ($dropdown.outerWidth() - $dropdown.width()) > 0 ? (($dropdown.outerWidth() - $dropdown.width())/2) : 0;

        // Add space for the close button on touch devices
        if ($('.mobile-nav-open').length > 0){
            closeBtnWidth = $('.mobile-overlay-close').width();
        }

        var x = (($dropdown.width() + dropdown_width + closeBtnWidth) + $dropdown.parent().position().left) - container_width;
        if (x > 0){
            $dropdown.css('margin-left', '-' + Math.ceil(x) + 'px');
        }
    }

    // Event handler functions

    // Removes the default onClick event handler for touch
    function handleTouchStart (event){
        event.preventDefault();
        this.onClick = {};
    }

    // Handle menu interaction
    function handleTouchEnd (event){
        // Check if the user was scrolling
        var changedTouch = event.changedTouches[0];
        var elem = document.elementFromPoint(changedTouch.clientX, changedTouch.clientY);
        if (elem !== this) return;

        clearMegaMenuCloseBtnStyling();
        var listItem = $(this).parent();
        var $dropdown = listItem.find('.sub');
        listItem.siblings('.open-sub-menu').removeClass('open-sub-menu');
        if (BootstrapFunctions.isBreakpoint(['md', 'lg'])){
            // Open or close this menu
            if (listItem.hasClass('open-sub-menu')){
                window.location.href = $(this).attr('href');
            } else {
                $('html').addClass('mobile-nav-open');
                listItem.removeClass('close-sub-menu').addClass('open-sub-menu');
                positionMegaMenu($dropdown);

                // Position the close button
                $('.mobile-overlay-close').css({
                    top: $dropdown.offset().top,
                    left: $dropdown.offset().left + Math.floor($dropdown.outerWidth()),
                    display:'block'
                });
            }
        } else {
            openMobileSubNav(listItem);
        }
    }

    // Mobile Search Button
    function handleMobileSearchToggleClick (){
        var search = $('.header-section-2 .block-3');
        search.toggleClass('show-search');
        if (search.hasClass('show-search')){
            $('.header-search-input').focus();
        } else {
            $('.header-search-input').blur();
        }
    }

    // Show Side nav
    function handleSideNavClick (){
        // Close any previously opened sub cats and remove any mega menu sub styling
        $('.open-mobile-sub, .open-mobile-sub .in').removeClass('open-mobile-sub in');
        $('.main-navigation .sub').removeAttr('style');
        $('html').addClass('mobile-nav-open');
    }

    // Toggle sub navigation categories
    function handleSubNavClick (){
        openMobileSubNav($(this).parent());
    }

    // Shift mega menu if it goes off page
    function handleMouseOver (){
        if (BootstrapFunctions.isBreakpoint(['md', 'lg'])){
            positionMegaMenu($(this).find('.sub'));
        }
    }

    // Dismiss side nav
    function handleOverlayClick (){
        closeMenu();
    }

    // Check if the $dropdown overflows out of the page and add the right align class if it does
    function offsetMenuDropdown (){
        var $dropdown = $(this).find('.dropdown-menu');
        var container_offset = $(this).parents('.header-inner').offset().left + $(this).parents('.header-inner').width();
        if ((($dropdown.width() + $(this).offset().left) - container_offset) > 0){
            $dropdown.addClass('dropdown-menu-right');
        }
    }

    // Resize
    $(window).on('resize', BootstrapFunctions.debounce(function(){ closeMenu(); }, 100));

    // These functions are exposed so that the demo server can rerun the header when the theme switcher is used
    window.headerMethods = { init: init, unbindEvents: unbindEvents };
    init();
});