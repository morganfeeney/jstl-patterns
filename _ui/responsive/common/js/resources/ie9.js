var ie9Modal = '<div class="modal fade" id="ie9Modal"><div class="modal-dialog"><div class="modal-content text-center"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><img src="http://i1.adis.ws/i/simplybe/INT-logo.gif" alt="Simply Be"></div><div class="modal-body modal-body-grey"><h3>Please upgrade your browser</h3><p>Hi, we\'ve noticed that your version of Internet Explorer is out of date.</p><p>Please upgrade your browser or use a suggested alternative below to enjoy the full Simply Be experience.</p><div class="row"><div class="col-xs-12"><a href="https://www.google.com/chrome/browser/desktop/"><img src="http://i1.adis.ws/i/simplybe/chrome.png" alt="Download Chrome"style="width:45px; height:45px;"></a><a href="https://www.mozilla.org/en-GB/firefox/new/"><img src="http://i1.adis.ws/i/simplybe/firefox.png" alt="Download Firefox" style="width:45px; height:45px;"></a><a href="http://windows.microsoft.com/en-gb/internet-explorer/download-ie"><img src="http://i1.adis.ws/i/simplybe/ie.png" alt="Download IE" style="width:45px; height:45px;"></a></div></div></div><div class="modal-footer"><div class="row"><div class="col-xs-12 col-phablet-p-4"><a class="btn btn-block btn-default" href="https://www.google.com/chrome/browser/desktop/">Google Chrome</a></div><div class="col-xs-12 col-phablet-p-4"><a class="btn btn-block btn-default" href="https://www.mozilla.org/en-GB/firefox/new/">Firefox</a></div><div class="col-xs-12 col-phablet-p-4"><a class="btn btn-block btn-default" href="http://windows.microsoft.com/en-gb/internet-explorer/download-ie">Internet Explorer</a></div></div></div></div></div></div>';

// generic get cookie function returnung value
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

// check for cookie on load - if not accepted show markup
if (getCookie('ie9') != 'accepted'){
	$(window).load(function(){
		$('body').append(ie9Modal);
		$('#ie9Modal').modal('show');
		document.cookie="ie9=accepted; expires=Sun, 31 Dec 2016 23:59:59 UTC";
	});
};