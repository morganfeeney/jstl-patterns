if (jQuery('#ampContainer').length > 0){
    window.ecommBridge = { 
        site:{
            "page":{
                "type":"product",
                "name":"media list",
                "mediaList": [
                    {"type":"i","name":"f01bp013022w_8002f21a"},
                    {"type":"i","name":"f01bp013023w_8002f21b"},
                    {"type":"i","name":"f01bp013024w_8002f21c"},
                    {"type":"i","name":"f01bp013026w_8002f21e"}
                ]
            }
        }
    };

    // Amplience Standard
    //if (init_amplience.viewer == "standard"){

    if (!WURFL.is_mobile){

        $('#ampContainer').addClass('ampStandardContainer');
        $.getScript("https://s1.adis.ws/viewers/standard/pdp/1.1.0/amp-standard-viewer-pdp-libs.min.js", function(){
            $.getScript("https://s1.adis.ws/viewers/standard/pdp-fullscreen/1.1.0/amp-fullscreen-viewer-pdp.min.js" )
            $.getScript("https://s1.adis.ws/viewers/standard/pdp/1.1.0/amp-standard-viewer-pdp.min.js", function (){
                var playerLayout = {
                    appendTo:$("#ampContainer"),
                    thumbLocation:"bottom",
                    numThumbs:4,
                    enableFullscreenMode:true,
                    showStackNavButtons:true,
                };

                var video = {
                    autoplay:true,
                    loopVideo:true
                };

                var spin = {
                    autoplay:true,
                    momentum:true
                };

                var zoom = {
                    zoomType:"flyout",
                    zoomActivation:"hover"
                };

                var aspectRatio = {
                    height: 1,
                    width: 1
                };

                var transformations = {
                    main: "$pdpmain_v2$",
                    thumb: "$pdpthumb$",
                    zoom: "$pdpzoom_v2$"
                };

                var accountConfig = {
                    client: "jdwilliams",
                    imageBasePath: "https://i1.adis.ws/",
                    contentBasePath: "https://c1.adis.ws/"
                };

                var viewer = new amp.StandardViewerPdp("#ampContainer", accountConfig, playerLayout, video, spin, zoom, transformations);
            });
        });
    // Amplience Mobile
    } else {
        $('#ampContainer').addClass('ampMobileContainer');
        $.getScript( "https://s1.adis.ws/viewers/standard/pdp-mobile/1.1.0/amp-mobile-viewer-pdp-libs.min.js", function(){
            $.getScript( "//s1.adis.ws/viewers/standard/pdp-mobile/1.1.0/amp-mobile-viewer-pdp.min.js", function(){
              var playerLayout = {
                appendTo:$("#ampContainer"),
                showThumbnails:true,
                numThumbnails:3,
                showThumbnailNavButtons:true,
                showStackNavButtons:true,
                showPaginationPoints:false
              };

              var transformations = {
                main: "$pdpmain_v2$",
                thumb: "$pdpthumb$",
                zoom: "$pdpzoom_v2$"
              };

              var accountConfig = {
                client: "jdwilliams",
                imageBasePath: "//i1.adis.ws/",
                contentBasePath: "//c1.adis.ws/"
              };

              var viewer = new amp.MobileViewerPdp("#ampContainer", accountConfig, playerLayout, transformations);
            });
        });
    }
}