$.fn.extend({
	clickWithDelay: function(callback, delay){

		var self = this;
		var clickDelay = 350;

		// Check if a delay has been supplied, validate it and override the default delay value
		if (delay !== null || delay !== undefined){
			if (!isNaN(parseFloat(delay)) && isFinite(delay)){
				clickDelay = delay;
			}
		}

		// Setup the click event
		$(self).on('click', function (){
			if ($(self).hasClass('clicked')) return;	
			$(self).addClass('clicked');
			var timer = setTimeout(function(){
				self.removeClass('clicked');
			}, clickDelay);
			callback.call(this);
			return false;
		});
	}
});