// Useful Bootstrap functions 
// https://bitbucket.org/giuseppepaul/useful-bootstrap-functions
(function(jQuery) {
	var BootstrapFunctions = function ($){
		var self = this;

		// Config vars
		var configs = {
			breakpointClasses: [
				{
					'id' : 'xs',
					'min': 0,
					'max': 589
				},
				{
					'id' : 'phablet-p',
					'min': 590,
					'max': 666
				},
				{
					'id' : 'phablet-l',
					'min': 667,
					'max': 767
				},
				{
					'id' : 'sm',
					'min': 768,
					'max': 991 
				},
				{
					'id' : 'md',
					'min': 992,
					'max': 1199
				},
				{
					'id' : 'lg',
					'min': 1200
				}
			],
			breakpoint: null,
			prevBreakpoint: null,
			useDebounce: false,
			debounceDelay: 100
		};

		// Init
		self.init = function () {
			// Setup elements for breakpoint methods
			for (var i = 0; i < configs.breakpointClasses.length; i++) {
				var elemClasses = 'device-' + configs.breakpointClasses[i]['id'] + ' visible-' + configs.breakpointClasses[i]['id'] + ' breakpoint';
				var elem = $('<div>').addClass(elemClasses).attr('data-breakpoint', configs.breakpointClasses[i]['id']);
				$('body').append(elem);				
			};
		}();

		// Get Current Breakpoint
		self.getBreakpoint = function () {
			var breakpoint = $('.breakpoint:visible').attr('data-breakpoint');
			return breakpoint;
		};

		// Get Breakpoint Min
		self.breakpointMin = function (id){
			for (var i = configs.breakpointClasses.length - 1; i >= 0; i--) {
				if (configs.breakpointClasses[i]['id'] === id){
					return configs.breakpointClasses[i]['min'];
				}
			};
			console.log('The breakpoint was not recognised.');
		};

		// Get Breakpoint Max
		self.breakpointMax = function (id){
			for (var i = configs.breakpointClasses.length - 1; i >= 0; i--) {
				if (configs.breakpointClasses[i]['id'] === id){
					return configs.breakpointClasses[i]['max'];
				}
			};
			console.log('The breakpoint was not recognised.');
		};		

		// Get Previous Breakpoint
		self.getPrevBreakpoint = function (){
			return configs.prevBreakpoint;
		}

		// Check if previous breakpoint
		self.isPrevBreakpoint = function (alias){
			if ($.isArray(alias)){
				for (var i = alias.length - 1; i >= 0; i--) {
					if (alias[i] === configs.prevBreakpoint) return true;
				};
			} else {
				if (alias === configs.prevBreakpoint) return true;
			}
			return false;
		}

		// Check if breakpoint
		self.isBreakpoint = function (alias) {
			var checkBreakpoint = false;

			if ($.isArray(alias)){
				for (var i = alias.length - 1; i >= 0; i--) {					
					checkBreakpoint = $('.device-' + alias[i]).is(':visible');
					if (checkBreakpoint === true) break;
				};
			} else {
				checkBreakpoint = $('.device-' + alias).is(':visible');
			}
		    return checkBreakpoint;
		};

		// Set breakpoint
		self.setBreakpoint = function (){
			configs.breakpoint = self.getBreakpoint();
		};

		// Debounce function
		self.debounce = function(fn,delay){
		    var timeoutId;
		    return function debounced(){
		        if(timeoutId){
		            clearTimeout(timeoutId);
		        }
		        timeoutId = setTimeout(fn.bind(this),delay);
		    }
		};

		// Monitor breakpoint changes
		self.monitorBreakpoints = function() {
    		if (self.getBreakpoint() != configs.breakpoint){

    			configs.prevBreakpoint = configs.breakpoint;
    			
    			// Set the breakpoint
    			self.setBreakpoint();
    			if (configs.prevBreakpoint == null) configs.prevBreakpoint = configs.breakpoint;

    			if (configs.breakpoint !== configs.prevBreakpoint){
	    			$('body').trigger('breakpointHit');
	    			$('body').trigger(configs.prevBreakpoint + '-breakpoint-exit');	    			
	    			$('body').trigger(configs.breakpoint + '-breakpoint-enter');
	    			$('body').trigger('exit-' + configs.prevBreakpoint + '-enter-' + configs.breakpoint);
	    			//console.log('exit-' + configs.prevBreakpoint + '-enter-' + configs.breakpoint)
    			}
    		}
		};

		// Check if groups of breakpoints were crossed
		self.didCross = function (collection1, collection2, viceversa){
			var vv = true;
			if (viceversa === false) vv = false;

			// Collection 1 is the previous breakpoint and collection 2 is the new breakpoint
			if (self.isPrevBreakpoint(collection1) && self.isBreakpoint(collection2)) return true;

			// Collection 2 is the previous breakpoint and collection 1 is the new breakpoint
			if (vv === true){
				if (self.isPrevBreakpoint(collection2) && self.isBreakpoint(collection1)) return true;
			}

			return false;
		};

		// Setup events for did cross check
		self.didCrossCheck = function (collection1, collection2, viceversa, callback){
			$('body').on('breakpointHit', function(){
				if (self.didCross(collection1, collection2, viceversa)){					
					callback();
				}	
			});
		};

		// Setup resize events
		if (configs.useDebounce === true){
			$(window).on('resize', self.debounce(self.monitorBreakpoints, self.configs.debounceDelay));
		} else {
			$(window).on("resize", self.monitorBreakpoints);
		}

		// Setup initial page load breakpoint
		//self.monitorBreakpoints();

		// Expose methods
    	return {
    		isBreakpoint: isBreakpoint,
    		getBreakpoint: getBreakpoint,
    		getPrevBreakpoint: getPrevBreakpoint,
    		isPrevBreakpoint: isPrevBreakpoint,
    		didCross: didCross,
    		didCrossCheck: didCrossCheck,
    		breakpointMax: self.breakpointMax,
    		breakpointMin: self.breakpointMin,
    		debounce:self.debounce
    	}
	};

	// Setup the global object and run init on document ready
	$(function(){ window.BootstrapFunctions = BootstrapFunctions(jQuery); });
})(jQuery);