$(function(){

	$('.marketing-primary-checkbox').change(function(){
		var childCheckboxes = $(this).parents('.marketing-prefs').find('.marketing-prefs-sub input[type="checkbox"]');
		if ($(this).prop('checked')) {
			childCheckboxes.prop('checked', true);
		}
		else {
			childCheckboxes.prop('checked', false);
		}
	});
	$('.marketing-prefs-sub input[type="checkbox"]').change(function(){
		var checkAll = $(this).parents('.marketing-prefs').find('.marketing-primary-checkbox');
		var checked_checkboxes = $(this).parents('.marketing-prefs').find('.marketing-prefs-sub input[type="checkbox"]:checked');
		if (checked_checkboxes.length > 0){
			checkAll.prop('checked', true);
		} else {
			checkAll.prop('checked', false);
		}
	});

});
