//
// modal-builder.js 
//

(function(jQuery) {
	
	var modalBuilder = function ($){
		
		// Check to see if there are any `btn-modal` classes in the DOM,
		// if not stop the function from going any further
		if ($('.btn-modal').length === 0) return false;

		// Set some global variables
		var self = this;
		self.modal = null;
		var $modalBuilder = $('.btn-modal');

		// Create a modal out of HTML following the Bootstrap markup pattern
		self.buildModalHtml = function(){
			var htmlString = 
				'<div class="modal fade modal-builder">'+
					'<div class="modal-dialog">'+
						'<div class="modal-content">'+
							'<div class="modal-body clearfix">'+
								'<div class="modal-inner col-xs-12 col-lg-12">'+
									'<p>One fine body&hellip;</p>'+
								'</div>'+
							'</div>'+
							'<div class="modal-footer clearfix">'+
								'<div class="col-xs-12 col-lg-12">'+
									'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
			self.modal = $(htmlString);			
		};

		self.buildModals = function (){
			$modalBuilder.on('click', function(e){
				e.preventDefault();

				// Build the modal HTML
				self.buildModalHtml();

				// Append the modal to the <body>
				$('body').append(self.modal);

				// Define the variables which will contain the data you want to display
				var type = $(this).attr('data-type');
				var contents = $(this).attr('data-content');

				// Check to see which data attributes hold which type of data
				switch (type) {
					case "iframe-survey" :
						self.modal.on('show.bs.modal',function(){
							self.modal.addClass('iframe-survey');
						});
						contents = '<iframe frameborder="0" src="'+ contents + encodeURIComponent(window.location.href) + '"></iframe>';
					break;
					case "html" :
						self.modal.on('show.bs.modal',function(){
							self.modal.addClass('html-content');
						});
						contents = (typeof contents == "undefined") ? "no data" : contents;
					break;
					default :
						contents = 'default';
					break;
				}

				// Insert the data from the relevant data attribute into the `contents` variable
				self.modal.find('.modal-inner').html(contents);

				// Display the modal
				$('.modal-builder').modal('show');

				// Hide and remove the modal from the DOM using the `removeModals()` function
				self.removeModals();
			});
		};
		
		self.removeModals = function (){
			self.modal.on('hidden.bs.modal', function () {
				self.modal.remove();
			});
		};

		self.buildModals();
	
		// Expose methods / properties
		return {
			buildModals: self.buildModals
		};
	};

	// Setup the global object on document ready
	$(function(){ 
		window.modalBuilder = new modalBuilder(jQuery);
	});

})(jQuery);