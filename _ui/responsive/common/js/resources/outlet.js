// Move the active tab to the end of its parent
function arrangeTabs($tabs){
	$tabs.find('.active').insertAfter($tabs.children().last());
};

function animateTab ($tab){
	$tab.slideUp(250, function(){
		arrangeTabs($(this).parents('.nav-tabs'));
	}).slideDown(250);
};

// Update position of active tab after tab change
$('.outlet-content-tabbed .nav-tabs li a').on('shown.bs.tab', function (){
	if (BootstrapFunctions.isBreakpoint('xs')){
		animateTab($(this));
	}
});

// Arrange the tabs on page load if on xs
$(function(){
	if (BootstrapFunctions.isBreakpoint('xs')){
		arrangeTabs($('.outlet-content-tabbed .nav-tabs'));
	}
	// On entering the xs breakpoint arrange the tabs
	$('body').on('xs-breakpoint-enter', function(){
		arrangeTabs($('.outlet-content-tabbed .nav-tabs'));
	});
});