$(function(){
	if (BootstrapFunctions.isBreakpoint(['xs', 'phablet-p'])){
		// Work around to snap the accordion shut without animation
		$('#parcel-destination-address').css('display', 'none').collapse('hide').removeAttr('style');
	}		
});