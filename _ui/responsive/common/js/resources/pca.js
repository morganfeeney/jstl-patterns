(function(jQuery) {
	var PCA = function ($){

		var self = this;

		self.configs = {
			"pcaActiveText" : "or enter manually",
			"pcaInactiveText" : "Use our address finder"
		}

		self.initPCA = function (){
			$('[data-pca-inputs]').each(function(){
				self.setupPCA($(this), false);
				$(this).bind('click', function(){
					self.setupPCA($(this), true);
					return false;
				});
			});
		};

		self.setupPCA = function ($PCALink, toggle){
			var pcaActive = $PCALink.attr('data-pca-active') == 'true' ? true : false;
			if (toggle) pcaActive = !pcaActive;

			if (pcaActive){
				// Set the active text
				$PCALink.find('.text').text(configs.pcaActiveText);
				// Hide the manual inputs
				$($PCALink.attr('data-pca-inputs')).hide();
				// Show the PCA Address box and PCA input
				$($PCALink.attr('data-pca-field')).show();
				$($PCALink.attr('data-pca-address')).show();
			} else {
				// Set the inactive text
				$PCALink.find('.text').text(configs.pcaInactiveText);
				// Hide the PCA Address box and PCA input
				$($PCALink.attr('data-pca-field')).hide();
				$($PCALink.attr('data-pca-address')).hide();
				// Show the manual inputs
				$($PCALink.attr('data-pca-inputs')).show();
			}

			// Update the pca active attribute
			if (toggle) $PCALink.attr('data-pca-active', pcaActive);
		}

		return {
			initPCA: self.initPCA
		}
	}

	// Setup the global object and run init on document ready
	$(function(){
		window.PCA = PCA(jQuery);
		window.PCA.initPCA();
		$('.pca-address-edit-btn').bind('click', function(){
			$('.pca-toggle-link').click(); return false;
		});
	});
})(jQuery);
