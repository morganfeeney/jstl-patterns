(function($) {

	// Acts as a bit of a service layer above owl sliders API
	var ImageZoom = function ($){
		var self = this;
		self.configs = {};

		// Carousel objects / configs
		self.carousels = {};
		self.LB = null;
		self.configs.zoomCarouselInitialized = false;

		self.init = function (){
			// Abort if the PDP image containers aren't found
			if ($('#sync1').length === 0 || $('#sync2').length === 0) return;

			// Setup references to the carousels
			self.carousels.mainCarousel = $('#sync1');
			self.carousels.thumbsCarousel = $('#sync2');

			// Setup the carousels
			self.setupCarousels();		
		};	

		self.setupCarousels = function (){
			// Setup the main carousel
			self.carousels.mainCarousel.owlCarousel({
				singleItem: true,
				lazyLoad: true,
				slideSpeed: 100,
				navigation: true,
				pagination:false,
				slideSpeed: 200,
				paginationSpeed: 200,
				rewindSpeed: 200,
				afterAction: self.syncCarousels,
				afterLazyLoad: function (){
					if (self.LB === null){
						self.configs.totalCarouselItems = self.carousels.mainCarousel.find('.product-image-main').length;
						self.setupLB();	
					}
				}
			});
			// Setup the thumbnails carousel
			self.carousels.thumbsCarousel.owlCarousel({
				items: 4,
				lazyLoad: true,
				itemsDesktop: [1199,4],
				itemsDesktopSmall: [979,4],
				itemsTablet: [768,4],
				itemsMobile: [479,4],
				navigation: true,
				pagination: false,
				paginationSpeed: 100,
				rewindSpeed: 200,
				afterInit: function(el){
					el.find('.owl-item').eq(0).addClass('synced');
				}
			});

			// Event to link the thumbnails to the main carousel
			self.carousels.thumbsCarousel.on('click', '.owl-item', function(e){
				e.preventDefault();
				var number = $(this).data('owlItem');
				self.carousels.mainCarousel.trigger('owl.goTo',number);
			});							
		};

		self.setupLB = function (){
			// Setup pop
			if (self.LB === null) self.buildLB();

			// Zoom event listener
			self.carousels.mainCarousel.on('click', '.owl-item', function (e){
				self.zoomClick(e);
			});
		};

		self.setupPincher = function (){
			// Setup zoom
			var pzVars = { 
				imageOptions:{ 
					scaleMode:'full', 
					maxZoom: 2
				} 
			};
			$('.owl-item.active img').pinchzoomer(pzVars);				
		};

		self.toggleLightbox = function (){
			if (self.configs.lb_visible){
				self.LB.css('display', 'none');
				$('html, body').removeAttr('style');
				self.configs.lb_visible = false;			
			} else {

				// Fix the body while the popup is open
				$('html, body').css({
					'height': $(window).innerHeight(),
					'width': $(window).innerWidth(),
					'overflow': 'hidden'
				});

				self.carousels.zoomCarousel.css('height', $(window).innerHeight());
				self.carousels.zoomCarousel.find('.owl-wrapper-outer').css('height', self.carousels.zoomCarousel.height());
				self.carousels.zoomCarousel.find('.owl-item').each(function(){
					$(this).css('height', self.carousels.zoomCarousel.height());
				});
				self.carousels.zoomCarousel.data('owlCarousel').jumpTo(self.getCurrentItem());
				self.updateLBSlideNumber();	
				self.LB.css('display', 'block');
				self.configs.lb_visible = true;
			}
		};

		self.updateLBSlideNumber = function (){
			self.LB.find('#jdw-lb-item-count .item-current').text(self.getCurrentItem(self.carousels.zoomCarousel) + 1); // Current item is zero based so + 1
		};

		self.zoomClick = function (e){
			// Setup the main carousel
			if (self.configs.zoomCarouselInitialized === false){
				self.carousels.zoomCarousel.owlCarousel({
					singleItem: true,
					lazyLoad: true,
					slideSpeed: 100,
					navigation: true,
					pagination:false,
					slideSpeed: 200,
					paginationSpeed: 200,
					rewindSpeed: 200,
					touchDrag: false,
					mouseDrag: false,
					addClassActive: true,
					afterUpdate: function (){
						self.setupPincher();
					},
					afterMove: function (){						
						self.setupPincher();
						self.updateLBSlideNumber();
					}
				});
				self.configs.zoomCarouselInitialized = true;
				self.LB.on('click', function(e){ 
					if ($(e.target).is('#jdw-lb')) self.toggleLightbox(); 
				});
			}
			self.toggleLightbox();
		};

		self.syncCarousels = function (el){
			var current = this.currentItem;
			self.carousels.thumbsCarousel.find('.owl-item').removeClass('synced').eq(current).addClass('synced');
			if(self.carousels.thumbsCarousel.data('owlCarousel') !== undefined){
				self.centerThumbs(current);
			}
		};

		self.centerThumbs = function (number){
			var sync2visible = self.carousels.thumbsCarousel.data('owlCarousel').owl.visibleItems;
			var num = number;
			var found = false;

			for(var i in sync2visible){
				if(num === sync2visible[i]){
					var found = true;
				}
			}

			if(found===false){
				if(num>sync2visible[sync2visible.length-1]){
					self.carousels.thumbsCarousel.trigger('owl.goTo', num - sync2visible.length+2)
				}else{
					if(num - 1 === -1){
						num = 0;
					}
					self.carousels.thumbsCarousel.trigger('owl.goTo', num);
				}
			} else if(num === sync2visible[sync2visible.length-1]){
				self.carousels.thumbsCarousel.trigger('owl.goTo', sync2visible[1])
			} else if(num === sync2visible[0]){
				self.carousels.thumbsCarousel.trigger('owl.goTo', num-1)
			}
		};

		self.getLargeSrc = function (x){
			return $(self.carousels.mainCarousel.find('.owl-item').eq(x)[0]).find('img.product-image-main').attr('data-lg-src');
		};

		self.getCurrentItem = function (carousel){
			carousel = typeof carousel !== 'undefined' ? carousel : self.carousels.mainCarousel;
			return carousel.data('owlCarousel').currentItem;
		};

		self.buildLB = function (){
			self.LB = $('<div id="jdw-lb-overlay">');
			self.carousels.zoomCarousel = $('<div id="jdw-lb">');

			var zoomTop = $('<div id="jdw-lb-top"><button id="jdw-lb-close-btn" onclick="jdw.imageZoom.toggleLightbox()" class="jdw-lb-btn"><button id="jdw-lb-zoom-in-btn" class="jdw-lb-btn">');
			var itemCount = $('<span id="jdw-lb-item-count"><span class="item-current"></span> / <span class="item-total">' + self.configs.totalCarouselItems + '</span></span>');

			zoomTop.append(itemCount);
			self.LB.append(zoomTop);			

			var items = self.carousels.mainCarousel.find('.product-image-main');
			items.each(function(){
				self.carousels.zoomCarousel.append('<img class="lazyOwl" data-src="' + $(this).attr('data-lg-src') + '">');
			});

			self.LB.append(self.carousels.zoomCarousel);
			$('body').append(self.LB);
			self.configs.lb_visible = false;
		};

		// Expose
		return {
			init: self.init,
			getCurrentItem: self.getCurrentItem,
			toggleLightbox: self.toggleLightbox
		}
	};

	$(function(){
		window.jdw = {}
		window.jdw.imageZoom = new ImageZoom($);
		window.jdw.imageZoom.init();
	});

})(jQuery);