// Personalised Input
function personalisedMessage() {
    var personalInput = document.getElementById('personaliseInput');
    if (personalInput !== null) {
        var maxLength = personalInput.maxLength;

        personalInput.onkeyup = function(){
            var left = maxLength - personalInput.value.length;
            if( personalInput.value.length > maxLength ){
                personalInput.value = personalInput.value.substring(0, maxLength );
                left = 0;
            }
            $('.personalised-counter').text(left);
        };
    }
}

// Repositions pricing
function movePrice() {
    if (BootstrapFunctions.isBreakpoint(['xs'])) {
        $('#PDP-price-mobile').append($('#PDP-price .pricebox'));
    } else {
        $('#PDP-price').append($('#PDP-price-mobile .pricebox'));
    }
}

// Repositions title
function moveTitle() {
    if (BootstrapFunctions.isBreakpoint(['xs'])) {
        $('#PDP-title-mobile').append($('#PDP-title .product-title'));
    } else {
        $('#PDP-title').append($('#PDP-title-mobile .product-title'));
    }
}

// Repositions ratings
function moveRatings() {
    if (BootstrapFunctions.isBreakpoint(['xs'])) {
        $('#PDP-ratings-mobile').append($('.PDP-ratings #BVRRSummaryContainer'));
    } else {
        $('.PDP-ratings').append($('#PDP-ratings-mobile #BVRRSummaryContainer'));
    }
}

// Repositions multibuy
function moveMultibuy() {
    if (BootstrapFunctions.isBreakpoint(['xs'])) {
        $('#PDP-multibuy-mobile').append($('#PDP-multibuy #accordion-multibuy'));
    } else {
        $('#PDP-multibuy').append($('#PDP-multibuy-mobile #accordion-multibuy'));
    }
}

function canHoverZoom(){
    // Only allow hover zoom if the 'hover-zoom-enabled' class is found in the html
    if ($('.hover-zoom-enabled').length === 0) return;
    var can_zoom = true;
    var active_slide = $('#pdp-carousel .owl-item.active');
    // Check if its a video or too small to hover zoom
    if (active_slide.find('.video-item').length > 0 || active_slide.find('.no-zoom').length > 0) can_zoom = false;
    return can_zoom;
}

function canFullScreenZoom(){
    return $('.full-screen-zoom-enabled').length === 0 ? false : true;
}

function syncPdpCarousels(el) {
    // Target desktops
    if (!WURFL.is_mobile) {
        // Check if a hover zoom exists & destroy it
        if ($('.amp-zoom').length > 0) {
            $('.amp-zoom').ampZoom('destroy');
        }
        // Setup the hover zoom
        // Check that it's not a video item
        if (canHoverZoom()){
            $('#pdp-carousel .owl-item.active').find('img').ampZoom({
                target: '#zoomBox',
                zoom: 3,
                preload: {
                    zoomed: 'create'
                },
                activate: {
                    mouse:'over'
                },
                cursor: {
                    active: 'none'
                }
            });
        }
    }

    // Sync carousels
    var current = this.currentItem;
    $("#pdp-thumbs-carousel").find(".owl-item").removeClass("synced").eq(current).addClass("synced")
    if ($("#pdp-thumbs-carousel").data("owlCarousel") !== undefined) {
        pdpThumbsCenter(current)
    }
}

function pdpThumbsCenter(number) {
    var sync2visible = thumbCarousel.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for (var i in sync2visible) {
        if (num === sync2visible[i]) {
            var found = true;
        }
    }

    if (found === false) {
        if (num > sync2visible[sync2visible.length - 1]) {
            thumbCarousel.trigger("owl.goTo", num - sync2visible.length + 2)
        } else {
            if (num - 1 === -1) {
                num = 0;
            }
            thumbCarousel.trigger("owl.goTo", num);
        }
    } else if (num === sync2visible[sync2visible.length - 1]) {
        thumbCarousel.trigger("owl.goTo", sync2visible[1])
    } else if (num === sync2visible[0]) {
        thumbCarousel.trigger("owl.goTo", num - 1)
    }
}

function centerPDPThumbs (){
	if (this.options.items > this.itemsAmount){
		this.$elem.find('.owl-wrapper').addClass('owl-center-items');
	}
	else {
		this.$elem.find('.owl-wrapper').removeClass('owl-center-items');
	}
}

$(function() {
    movePrice();
    $('body').bind('breakpointHit', movePrice);

    moveTitle();
    $('body').bind('breakpointHit', moveTitle);

    moveRatings();
    $('body').bind('breakpointHit', moveRatings);

    moveMultibuy();
    $('body').bind('breakpointHit', moveMultibuy);

    personalisedMessage();

    // PDP disabled button tooltip
    $('.btn-grid-disabled').tooltip({placement: "bottom"});

    // Main PDP image thumbnail carousel items
    var carouselItemNumbers = {
        'xs': 3,
        'phablet-p': 3,
        'phablet-l': 3,
        'sm': 3,
        'md': 4,
        'lg': 5
    };

    // PDP Main images
    mainCarousel = $('#pdp-carousel').owlCarousel({
        'lazyLoad': true,
        'singleItem': true,
        'addClassActive': true,
        'navigation': true,
        'pagination': false,
        'afterAction': syncPdpCarousels,
        'afterInit' : function (){
          $('#pdp-carousel').removeClass('hidden');
        }
    });

    // Thumbs
    thumbCarousel = $('.pdp-thumbs-carousel').owlCarousel({
        'navigation': true,
        'pagination': false,
        'itemsCustom': [
            [BootstrapFunctions.breakpointMin('xs'), carouselItemNumbers['xs']],
            [BootstrapFunctions.breakpointMin('phablet-p'), carouselItemNumbers['phablet-p']],
            [BootstrapFunctions.breakpointMin('phablet-l'), carouselItemNumbers['phablet-l']],
            [BootstrapFunctions.breakpointMin('sm'), carouselItemNumbers['sm']],
            [BootstrapFunctions.breakpointMin('md'), carouselItemNumbers['md']],
            [BootstrapFunctions.breakpointMin('lg'), carouselItemNumbers['lg']]
        ],
        'afterInit': function(el) {
            $('.pdp-thumbs-carousel').removeClass('hidden');
            el.find(".owl-item").eq(0).addClass("synced");
            centerPDPThumbs.call(this);
        },
        'afterUpdate' : function (){
            centerPDPThumbs.call(this);
        }
    });

    $(".pdp-thumbs-carousel").on("click", ".owl-item", function(e) {
        e.preventDefault();
        var number = $(this).data("owlItem");
        mainCarousel.trigger("owl.goTo", number);
    });

    // Fire click on the video link on the main image
    $('.pdp-main-carousel .pdp-img-zoom, .pdp-main-carousel .video-item').on('click', function() {
        if (canFullScreenZoom()){
            $('.pdp-main-carousel').addClass('load-zoom');
            window.location.href = $('#pdp-carousel .owl-item.active').find('a').attr('href');
        }
    });

    // Fire click on the video link for thumbnails
    $('.pdp-thumbs-carousel .video-item').on('click', function(e) {
        if (canFullScreenZoom()){
            $('.pdp-main-carousel').addClass('load-zoom');
            window.location.href = $(this).find('a').attr('href');
            e.stopPropagation();
        }
    });

    $('.pdp-main-carousel').on('click', 'a', function(){
        if (!canFullScreenZoom()) return false;
        $('.pdp-main-carousel').addClass('load-zoom');
    });


    // PDP Plus & Minus quanitity buttons
    $('.btn-number').click(function(e){
        e.preventDefault();

        var fieldName = $(this).attr('data-field');
        var type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var inputMaxLength = parseInt(input.attr('max'));
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                var minValue = parseInt(input.attr('min'));
                if(!minValue) minValue = 1;
                if(currentVal > minValue) {
                    input.val(currentVal - 1).change();
                }
                if(parseInt(input.val()) == minValue) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {
                var maxValue = parseInt(input.attr('max'));
                if(!maxValue) maxValue = inputMaxLength;
                if(currentVal < maxValue) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == maxValue) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
       $(this).data('oldValue', $(this).val());
    });
    // Set the Min / Max
    $('.input-number').change(function() {

        var minValue =  parseInt($(this).attr('min'));
        var maxValue =  parseInt($(this).attr('max'));
        if(!minValue) minValue = 1;
        if(!maxValue) maxValue = inputMaxLength;
        var valueCurrent = parseInt($(this).val());

        var name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            $(this).val($(this).data('oldValue'));
        }
    });
    // Set key presses
    $(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape and enter
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
    });

    // function togglePlusMinusSize(){
    //     if (BootstrapFunctions.isBreakpoint (['phablet-p', 'phablet-l', 'sm', 'md', 'lg'])) {
    //         $(".quantity-input button[data-type='minus']").removeClass(' btn-lg');
    //         $(".quantity-input button[data-type='plus']").removeClass(' btn-lg');
    //         $(".quantity-input input[data-value='quantity']").removeClass(' input-lg');
    //     }
    //     else if (BootstrapFunctions.isBreakpoint ('xs')) {
    //         $(".quantity-input button[data-type='minus']").addClass('btn-lg');
    //         $(".quantity-input button[data-type='plus']").addClass('btn-lg');
    //         $(".quantity-input input[data-value='quantity']").addClass('input-lg');
    //     }
    // }
    // togglePlusMinusSize();
    // $('body').bind('breakpointHit', togglePlusMinusSize);

    // Swap PDP grid .active class
    var $activeGridBtn = $('.grid-btns .btn-grid');
    function pdpGridActive(){
        $activeGridBtn.on('click',function(e){
            $('.active',$(this).closest('div')).removeClass('active');
            $(this).addClass('active');
        });
    }
    pdpGridActive();
    // Swap PDP grid .active class - END
});
