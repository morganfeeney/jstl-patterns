$(function(){
	function dropDownTextChanger(){
		$menuItem.on('click',function(e){
			$(this).closest('ul').siblings('button.dropdown-toggle').html(
				$(this).data('value')
			);
			e.preventDefault();
		});
	}

	function continueToCheckout(){
		$('[data-value=continue-to-checkout],.total-price').removeClass('hide').addClass('fade');
		setTimeout(function() {
			$formDropdown.removeClass('has-error');
        	$('[data-value=continue-to-checkout],.total-price').addClass('in');
      	}, 0);
	}

	function collapseBagIn() {
		$( "#bag-confirm" ).slideDown(300, function() {
		    $( "#add-to-bag-upsell" ).delay(350).addClass('open');
		});
	}
	function collapseBagOut() {
		$( "#add-to-bag-upsell" ).removeClass('open', function() {
			$( "#bag-confirm" ).delay(300).slideUp(300);
		});
	}

	// Dropdown variables
	var $menuItem = $('.dropdown-toggle + ul li a');
	var $menuItemQty = $('[data-value=quantity]');

	// Validation variables
	var $indicate = $('.stock-indicator');
	var $error = $('.notify.notify-error').not('.wishlist-error');
	var $wishlistError = $('.wishlist-error');
	var $success = $('.notify-success');
	var $formDropdown = $('.pseudo-dropdown').closest('.form-group');
	var $addToBag = $('[data-value=add-to-bag]');
	var $addToWishlist = $('[data-value=add-to-wishlist]');
	var $submit = $('[data-value=save-contact-details],[data-value=signin-existing],[data-value=signin-new],[data-value=contact-form],[data-value=submit]');
	var $formGroup = $('.required').not('[for=name-input]').closest('.form-group');

	function dropDownTextChanger(){
		$menuItem.on('click',function(e){
			$(this).closest('ul').siblings('button.dropdown-toggle').html(
				$(this).attr('data-value')
			).closest('.form-group').addClass('selected').removeClass('has-error');
			e.preventDefault();
		});
	}

	function phoneyBolonyValidation(){

		// Add to bag validation
		$addToBag.on('click',function(e){
			if (!$indicate.hasClass('in')) {
				$formDropdown.not('.selected').addClass('has-error');
				$indicate.add($error).removeClass('hide').addClass('fade');
					setTimeout(function() {
						$indicate.add($error).addClass('in');
					}, 0);
			} else if( $(".personalise-product").is(':visible') ) {
				if ($('#personaliseInput').val().length === 0) {
					$('#PersonalisedModal').modal('show');
				} else {
					continueToCheckout();
					$("html, body").animate({scrollTop: 0 }, 300, function() {
						collapseBagIn();
					});
				}
			} else if ( $(".breakdown-plan-btn").is(':visible') ) {
				continueToCheckout();
				$("html, body").animate({scrollTop: 0 }, 300, function() {
					collapseBagIn();
					if ($success.hasClass('breakdown-success')) {
						$('.breakdown-modal-panel.active').removeClass('active');
						$(".breakdown-modal-panel[data-panel='options']").addClass('active');
				        $('#BreakdownPlanModal').modal('show');
					}
				});
			} else {
				continueToCheckout();
			}

			e.preventDefault();
		});

		// Wishlist link validation
		$addToWishlist.on('click',function(e){
			if (!$indicate.hasClass('in')) {
				$formDropdown.not('.selected').addClass('has-error');
				$indicate.add($wishlistError).removeClass('hide').addClass('fade');
					setTimeout(function() {
						$indicate.add($wishlistError).addClass('in');
					}, 0);
			}
			e.preventDefault();
			var count = $('.selected').length;
			if ($formDropdown.hasClass('selected') && count === 5) {
				$('.text-tip-outer').collapse('show');
			}
		});

		// Pseudo dropdown validation
		$($menuItemQty).on('click',function() {
			if ($(this).parent().attr('class') == "disabled") {
				$('.stock-indicator,.notify-no-success,.total-price').removeClass('hide').addClass('fade');
				setTimeout(function() {
	            	$('.stock-indicator,.notify-no-success,.total-price').addClass('in');
	          	}, 0);
			} else {
				$('.stock-indicator,.notify.notify-error,.notify-no-success').removeClass('fade in').addClass('hide');
				$('.form-group').removeClass('has-error');
				$('.stock-indicator,.notify-success,.total-price').removeClass('hide').addClass('fade');
				setTimeout(function() {
	            	$('.stock-indicator,.notify-success,.total-price').addClass('in');
	          	}, 0);
	         }
		});

		// Add to bag validation
		$submit.on('click',function(e){
			if (!$indicate.hasClass('in')) {
				$formGroup.addClass('has-error');
				$indicate.add($error).removeClass('hide').addClass('fade');
					setTimeout(function() {
						$indicate.add($error).addClass('in');
					}, 0);
			}
			e.preventDefault();
		});
	}

	$('.btn-continue-shopping, .icon-close-upsell').click(function(){
		$("html, body").animate({scrollTop: 0 }, 300, function() {
			collapseBagOut();
		});
	});

	$('#personalised-continue').click(function(){
		$('#PersonalisedModal').modal('hide'); 
		continueToCheckout();
		$("html, body").animate({scrollTop: 0 }, 300, function() {
			collapseBagIn();
		});
	});


	dropDownTextChanger();
	phoneyBolonyValidation();
});
