$(function(){
	$('.product-carousel').owlCarousel({
		items : 5,
		itemsDesktop : [992,4], // lg
		itemsDesktopSmall : [768,3], // md
		itemsTablet: [480,2], // sm
		itemsMobile : false, // xs
		'afterInit': function (){
			centerProductCarousels.call(this);
		},
		'afterUpdate': function (){
			centerProductCarousels.call(this);
		}
	});

    $(".carousel-btn-next").click(function(){
		var slider = $(this).parents('.carousel-body').find('.product-carousel');
		slider.trigger('owl.next');
  	});

	$(".carousel-btn-prev").click(function(){
		var slider = $(this).parents('.carousel-body').find('.product-carousel');
  	  	slider.trigger('owl.prev');
  	});
});

function centerProductCarousels (){
	if (this.options.items > this.itemsAmount){
		this.$elem.parents('.custom-carousel').find('.carousel-btn').hide();
		this.$elem.find('.owl-wrapper').addClass('owl-center-items');
	}
	else {
		this.$elem.parents('.custom-carousel').find('.carousel-btn').show();
		this.$elem.find('.owl-wrapper').removeClass('owl-center-items');
	}
}

//If tabbed Click Functions
$(".carousel-tabbed .product-carousel-item").click(function(){
	// Index of clicked item
	var clickedIndex = $(this).parent().index();

	// Get the Slider
	var slider = $(this).parents('.carousel');

	// Get the Slider Tabs
	var tabPanels = $(this).parents('.carousel-body').find('.product-carousel-tabs');
	var newActiveTab = tabPanels.find('.product-carousel-tab-item').eq(clickedIndex);


	//Removes class from slider items as owl divs prevent default tab functionality.
	$(this).parents('.carousel-body').find('.product-carousel-item').not(this).removeClass('active');
	$(this).addClass('active');
	$('html, body').animate({
		scrollTop: $(tabPanels).offset().top
	}, 500);
	// Set the active tab
	if (!newActiveTab.hasClass('active-product')){
		tabPanels.find('.active-product').removeClass('active-product');
		newActiveTab.addClass('active-product');
	}

});

//Close Carousel Tab Content
$(".product-carousel-tab-item .icon-close").click(function(){
	var slider = $(this).parents('.carousel');
	$(this).parents(slider).find('.product-carousel-item').removeClass('active');
	$(this).parents('.product-carousel-tab-item').removeClass('active-product');
	$('html, body').animate({
		scrollTop: $(slider).offset().top
	}, 500);
});
