$(function(){

	// Category Filter 'Read more - Show less' buttons
	function toggleFilterviews(){
		$('#collapse-new-in ul#filter-new-in').hideMaxListItems({
			'max':6,
			'speed':0,
			'moreText':'<span class="glyphicon glyphicon-collapse-down"></span> View more new in ([COUNT])',
			'lessText':'<span class="glyphicon glyphicon-collapse-up"></span> View less'
		});
		$('#collapse-dress-style ul#filter-dress-style').hideMaxListItems({
			'max':6,
			'speed':0,
			'moreText':'<span class="glyphicon glyphicon-collapse-down"></span> View more dress styles ([COUNT])',
			'lessText':'<span class="glyphicon glyphicon-collapse-up"></span> View less'
		});
		$('#collapse-dress-size ul#filter-dress-size').hideMaxListItems({
			'max':6,
			'speed':0,
			'moreText':'<span class="glyphicon glyphicon-collapse-down"></span> View more dress sizes ([COUNT])',
			'lessText':'<span class="glyphicon glyphicon-collapse-up"></span> View less'
		});
		$('#collapse-dress-length ul#filter-dress-length').hideMaxListItems({
			'max':6,
			'speed':0,
			'moreText':'<span class="glyphicon glyphicon-collapse-down"></span> View more dress lengths ([COUNT])',
			'lessText':'<span class="glyphicon glyphicon-collapse-up"></span> View less'
		});
		$('#collapse-sleeve-length ul#filter-sleeve-length').hideMaxListItems({
			'max':6,
			'speed':0,
			'moreText':'<span class="glyphicon glyphicon-collapse-down"></span> View more sleeve lengths ([COUNT])',
			'lessText':'<span class="glyphicon glyphicon-collapse-up"></span> View less'
		});
		$('#collapse-colour ul#filter-colour').hideMaxListItems({
			'max':8,
			'speed':0,
			'moreText':'<span class="glyphicon glyphicon-collapse-down"></span> View more colours ([COUNT])',
			'lessText':'<span class="glyphicon glyphicon-collapse-up"></span> View less'
		});
		$('#collapse-price ul#filter-price').hideMaxListItems({
			'max':8,
			'speed':0,
			'moreText':'<span class="glyphicon glyphicon-collapse-down"></span> View more prices ([COUNT])',
			'lessText':'<span class="glyphicon glyphicon-collapse-up"></span> View less'
		});
	}
	toggleFilterviews();

	//Move the Filter Navigation to it's desktop location once the browser window uses the breakpoint ['sm', 'md', 'lg'].
	//Move the Filter Navigation to it's mobile location once the browser window uses the breakpoint ['xs', 'phablet-p', 'phablet-l'].
	function filterNavLayout(){
		if (BootstrapFunctions.isBreakpoint (['sm', 'md', 'lg'])) {
	        $('.mobile-filter-container .filter-navigation' ).detach().appendTo('.desktop-filter-container');
	    }
	    else if (BootstrapFunctions.isBreakpoint (['xs', 'phablet-p', 'phablet-l'])) {
	        $('.desktop-filter-container .filter-navigation').detach().appendTo('.mobile-filter-container');
	    }
	}
	filterNavLayout();
	$('body').bind('breakpointHit', filterNavLayout);

	//Buttons for swapping the styles for the viewing the mobile grid / list view of the product lister image fragment
	function toggleGridList(){
		if (BootstrapFunctions.isBreakpoint (['phablet-p', 'phablet-l', 'sm', 'md', 'lg'])) {
	        $("button[data-mobile-grid-list='grid']").removeClass('active');
	        $("button[data-mobile-grid-list='list']").removeClass('active');
	    }
	    else if (BootstrapFunctions.isBreakpoint ('xs')) {
	        $("button[data-mobile-grid-list='list']").removeClass('active');
	        $("button[data-mobile-grid-list='grid']").addClass('active');
	    }
		$("button[data-mobile-grid-list='grid']").click(function(e) {
		    $('.product-fragment').removeClass('col-xs-12').addClass('col-xs-6');
		    $(this).addClass('active');
		    $("button[data-mobile-grid-list='list']").removeClass('active');
		    e.preventDefault();
		});
		$("button[data-mobile-grid-list='list']").click(function(e) {
		    $('.product-fragment').removeClass('col-xs-6').addClass('col-xs-12');
		    $(this).addClass('active');
		    $("button[data-mobile-grid-list='grid']").removeClass('active');
		    e.preventDefault();
		});
	}
	toggleGridList();
	$('body').bind('breakpointHit', toggleGridList);

	//Reset the Mobile Grid/List to the default grid view once browser width has passed the mobile breakpoint into the desktop breakpoints
	function toggleGridListView(){
		if (BootstrapFunctions.isBreakpoint (['phablet-p', 'phablet-l', 'sm', 'md', 'lg'])) {
	        $('.product-fragment').removeClass( "col-xs-12" ).addClass( "col-xs-6" );
	    }
	}
	toggleGridListView();
	$('body').bind('breakpointHit', toggleGridListView);

	// //Swap the styles for the "Sort By" button when you get to the xs mobile view.
	// function toggleSortbySize(){
	// 	if (BootstrapFunctions.isBreakpoint (['sm', 'md', 'lg'])) {
	//         $('.sort-by').removeClass( "col-xs-5" ).addClass( "col-xs-4" );
	//     }
	//     else if (BootstrapFunctions.isBreakpoint (['xs', 'phablet-l', 'phablet-p'])) {
	//         $('.sort-by').removeClass( "col-xs-4" ).addClass( "col-xs-4" );
	//     }
	// }
	// toggleSortbySize();
	// $('body').bind('breakpointHit', toggleSortbySize);

	//Swap the styles for the "Product Per Page" button when you get to the xs mobile view.
	// function toggleProductPerPage(){
	// 	if (BootstrapFunctions.isBreakpoint (['sm', 'md', 'lg'])) {
	//         $('.products-per-page').removeClass( "col-xs-4" ).addClass( "col-xs-5" );
	//     }
	//     else if (BootstrapFunctions.isBreakpoint (['phablet-l', 'phablet-p'])) {
	//         $('.products-per-page').removeClass( "col-xs-5" ).addClass( "col-xs-5" );
	//     }
	// }
	// toggleProductPerPage();
	// $('body').bind('breakpointHit', toggleProductPerPage);

	// //Swap the styles for the "Refine" button when you get to the sm mobile view.
	// function toggleRefineSize(){
	//     if (BootstrapFunctions.isBreakpoint (['phablet-l', 'phablet-p'])) {
	//         $('.refine-filter').removeClass( "col-xs-3" ).addClass( "col-xs-3" );
	//     }
	//     else if (BootstrapFunctions.isBreakpoint ('xs')) {
	//         $('.refine-filter').removeClass( "col-xs-3" ).addClass( "col-xs-4" );
	//     }
	// }
	// toggleRefineSize();
	// $('body').bind('breakpointHit', toggleRefineSize);

	// Product fragment bauble - video available
	$('[data-toggle="video-tooltip"]').tooltip();
});

// Add event listeners on load
$(function(){
	// On collapse - hide
	$('.filter-navigation .panel-collapse').on('hide.bs.collapse', function (){
		// If there are no checkboxes selected then remove the selected class from the heading on close
		// and replace the tick with a caret if we're on mobile
		if (!refinementGroupActiveSelection(this)) {
			$(this).prev('.panel-heading').removeClass('selected');
			if (BootstrapFunctions.isBreakpoint(['xs', 'phablet-p', 'phablet-l'])){
				setToggleIcon('caret', this);
			}
		}
	});
	// On collapse - show
	$('.filter-navigation .panel-collapse').on('show.bs.collapse', function (){
		$(this).prev('.panel-heading').addClass('selected');
		if (BootstrapFunctions.isBreakpoint(['xs', 'phablet-p', 'phablet-l'])){
			setToggleIcon('tick', this);
		}
	});
	renderRefinements();
	$('body').bind('breakpointHit', renderRefinements);
});

// Lays out the refinements for different device sizes
function renderRefinements (){
	var refinementGroups = $('.filter-navigation .panel-group .panel-collapse');
	var renderView = BootstrapFunctions.isBreakpoint (['sm', 'md', 'lg']) ? 'desktop' : 'mobile';

	// Loop through each refinement group
	refinementGroups.each(function(){
		if (renderView === 'desktop'){
			// Everything should be open on desktop, if it's closed - open it
			if (!$(this).hasClass('in')) $(this).collapse('show');
			// Replace any ticks with carets
			setToggleIcon('caret', this);
		} else {
			// If the group has something checked - it should be open on a mobile
			if (refinementGroupActiveSelection(this)) {
				// If it's not open - open it
				if (!$(this).hasClass('in')) $(this).collapse('show');
				// Replace any arrows with ticks on items that are active
				setToggleIcon('tick', this);
			} else {
				// Or close it
				$(this).collapse('hide');
				setToggleIcon('caret', this);
			}
		}
	});
}

// Puts the right icon on the refinement group
function setToggleIcon (icon, refinementGroup){
	switch (icon){
		case 'tick':
			$(refinementGroup).prev('.panel-heading').find('a').addClass('filter-nav-selection');
		break;
		case 'caret':
			$(refinementGroup).prev('.panel-heading').find('a').removeClass('filter-nav-selection');
		break;
	}
}

// Checks if any items in a refinement group are selected
function refinementGroupActiveSelection (refinementGroup){
	var isselected = false;
	var $refinementGroup = $(refinementGroup);

	$refinementGroup.find('li.custom-checkbox input').each(function() {
		var $cur_this = $(this);
		if ($cur_this.is(':checked')) {
			isselected = true;
		}
	});
	return isselected;
}
