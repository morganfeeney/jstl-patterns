// Prevent spaces being entered in promo code input
$(function(){
  $('#promo-code').on('keydown', function(e) {
    return e.which !==32;
  });
});
