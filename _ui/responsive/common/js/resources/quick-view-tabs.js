function showHideTabNav() {
    var activeTab = $('.quick-view-tabs .nav-tabs li.active');
	var next = $('.btnNext');
	var prev = $('.btnPrevious');

	if (activeTab.is(':first-child')){
    	//hide prev
    	prev.hide();
    	// show next
    	next.show();
    } else if (activeTab.is(':last-child')) {
		// hide next
		next.hide();
		// show prev
		prev.show();
    } else {
		// show both
		next.show();
		prev.show();
	}
}
showHideTabNav();

$('.btnNext').click(function(){
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
    showHideTabNav();
});
$('.btnPrevious').click(function(){
    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    showHideTabNav();
});
$('.quick-view-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    showHideTabNav();
});
