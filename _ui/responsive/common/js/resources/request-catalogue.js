$('.req-cat-add-new-address-btn').click(function(){
	$('.address-selection-checkbox input[type="radio"').removeAttr('checked');
	$('.req-cat-customer-form-details').slideToggle();
});

$('.address-selection-checkbox input[type="radio"').click(function(){
	$('.req-cat-customer-form-details').hide();
});

$('#req-cat-create-account-link').click(function(){
	$('html, body').animate({
		scrollTop: $('#req-cat-create-account-form').offset().top
	}, 300);
	return false;
});
