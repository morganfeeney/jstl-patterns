$("input[name=itemType]:radio").change(function(){
    if($("#itemTypeYes").is(':checked')) {
        $(".returns-form").collapse('show');
        $(".returns-telephone").collapse('hide');
    } else {
        $(".returns-form").collapse('hide');
        $(".returns-telephone").collapse('show');
    }
});

$("#return-instructions").change(function(){
    if($(this).val() === "other") {
        $(".return-instructions").collapse('show');
    } else {
        $(".return-instructions").collapse('hide');
    }
});
