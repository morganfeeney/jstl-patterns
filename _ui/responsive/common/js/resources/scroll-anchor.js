$(function(){
    var speed = 0.4;
    $('.scroll-anchor').on('click', function(event){
        event.preventDefault();
        var targetPos = $($(this).attr('href')).offset().top;
        var currentPos = $(window).scrollTop();
        var distance = 0;

        if (targetPos < currentPos){
            distance = currentPos - targetPos;
        }
        else if (targetPos > currentPos){
            distance = targetPos - currentPos;
        }
        $('body,html').animate({ scrollTop: targetPos }, speed * distance);
    });
});