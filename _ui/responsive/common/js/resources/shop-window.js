$(function() {
	$(window).on('resize', BootstrapFunctions.debounce(function(){
		if(BootstrapFunctions.isBreakpoint(['sm', 'md', 'lg'])){
			var shopWindowHeight = $('.shop-window-cat-links').outerHeight();
			$('.shop-window-content, .shop-window-content-image').outerHeight(shopWindowHeight);
		} else {
				$('.shop-window-content, .shop-window-content-image').css('height', '250px');
		};
	}, 50)).resize();
});
 
