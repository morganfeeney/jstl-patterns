$(function(){
	// Scroll to form function
	function scrollToForm(){
		$('html, body').animate({
			scrollTop: $('#new-customer-signin-form').offset().top
		}, 300);
	}
	// Scroll to the form when toggled
	$('#new-head').click(function(){
			scrollToForm();
	});
	// Toggle the form and scroll when error message link is clicked
	$('#signin-create-new-account').click(function(){
			$('#new-form').collapse('show');
			scrollToForm();
			return false;
	});
	// Toggle the placement of the signin additional page links for mobile
	function moveSigninLinks(){
		if (BootstrapFunctions.isBreakpoint (['sm', 'md', 'lg'])) {
	        $('.signin-links-mobile .signin-links' ).detach().appendTo('.signin-links-desktop');
	    }
	    else if (BootstrapFunctions.isBreakpoint (['xs', 'phablet-p', 'phablet-l'])) {
	        $('.signin-links-desktop .signin-links').detach().appendTo('.signin-links-mobile');
	    }
	}
	moveSigninLinks();
	$('body').bind('breakpointHit', moveSigninLinks);
});
