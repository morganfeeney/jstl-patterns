$(function(){
	$(window).on('resize', BootstrapFunctions.debounce(function(){
		setStickyWidths();
	}, 300));

	$('#modal-size-guide a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	  setStickyWidths();
	});

	$('#modal-size-guide').on('shown.bs.modal', function(e){
		if($(this).find('.fixed-column').length===0){
			stickyTables($(this).find('.table-sticky-col-wrapper'));
		};
		setStickyWidths();
		$(this).find('.table-sticky-col-wrapper').css('visibility', 'visible');
		scrollIndicator($(this).find('.table-sticky-col-wrapper'));
	});
});

function scrollIndicator(currentTable){
	var availableWidth = $('#modal-size-guide .tab-pane').innerWidth();
	var tableInner = $(currentTable).find('.table-sticky-col-inner');
	// For some reason 'scrollWidth' doesn't seem to calculate padding. Adding it here as a variable to remove it in the next step
	var tabPadding = parseInt($('#modal-size-guide .tab-pane').css('padding'));
	tableInner.each(function(){
		if(this.scrollWidth > availableWidth - (tabPadding * 2)){
			var wrapper = $(this).parent().addClass('willScroll');
			$(this).scroll(function(){
				wrapper.addClass('scrolled').removeClass('willScroll');
			});
		}
	});
}

function stickyTables(tableWrapper){
	var $singleTableWrapper = $(tableWrapper);
	$singleTableWrapper.each(function(){
		var $table = $(this).find('table');
		var $fixedColTable = $table.clone();
		$fixedColTable.find('th:not(:first-child),td:not(:first-child)').remove();
		$fixedColTable.addClass('fixed-column').removeClass('table-sticky-col');
		$fixedColTable.insertBefore($table.parent());
		setStickyWidths();
	});
}

function setStickyWidths(){
	var $tables = $('.table-sticky-col-wrapper .fixed-column');
	$tables.each(function(){
		$(this).find('td').first().outerWidth(getColSize(this));
	});
}

function getColSize(table){
	return $(table).parent().find('.table-sticky-col td').first().outerWidth();
}
