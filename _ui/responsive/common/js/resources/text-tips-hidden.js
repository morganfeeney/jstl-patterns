(function($) {

	var textTips = function ($){
		var hiddenTips = function() {
			var textTip = $('.text-tip-outer');

			textTip.each(function(){
				var $this = $(this);
				var inputField = $this.parent().find('.form-control');

				inputField.on('focus', function(){
					if ($this.hasClass('multi') && $('.multi.in').length !== 0 ){
						$('.multi').collapse('hide')
					}
					$this.collapse('show');
				});
			});
		};

		// Expose the hiddenTips variable
		return {
			hiddenTips:hiddenTips
		}
	}
	window.textTips = new textTips($);

	// Setup the global object on document ready
	$(function(){ 
		window.textTips.hiddenTips();
	});

})(jQuery);