// text-tips.js 
//
// This script enables the use of popovers / modals, depending upon viewport width.
// It hooks into the isBreakpoint() function to determine said widths.

(function(jQuery) {
	
	var textTips = function ($){
		
		if ($('.btn-tip').length === 0) return false;

		var self = this;
		self.modal = null;

		self.configs = {
			'hasLoaded' : false,
			'currentFormat' : null
		};

		// Returns the current format - Mobile / Desktop
		self.getFormat = function (){
			if (BootstrapFunctions.isBreakpoint(['xs', 'phablet-p'])){
				return "Mobile";
			} else {
				return "Desktop";
			}
		};	

		// Create a modal 
		self.buildModal = function (content){
			var htmlString = 
				'<div class="modal fade text-tip-modal">'+
					'<div class="modal-dialog">'+
						'<div class="modal-content">'+
							'<div class="modal-body clearfix">'+
								'<div class="modal-inner col-xs-12 col-lg-12">'+
									'<p>One fine body&hellip;</p>'+
								'</div>'+
							'</div>'+
							'<div class="modal-footer clearfix">'+
								'<div class="col-xs-12 col-lg-12">'+
									'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
			self.modal = $(htmlString);			
		};

		self.removeModal = function (){
			$('.text-tip-modal').modal('hide');
			self.modal.on('hidden.bs.modal', function () {
			  self.modal.remove();
			})
		};

		// Positions form tips above or to the right of inputs
		self.positionTips = function (){
			// If this isn't initial doc load
			if (self.configs.hasLoaded === true) {
				// Check if the format needs updating - return if not
				if (self.configs.currentFormat === self.getFormat()) return
				// else update the format
				self.configs.currentFormat = self.getFormat();				
			}

			// If mobile 
			if (self.configs.hasLoaded === false && self.getFormat() == "Mobile" || self.configs.currentFormat == "Mobile"){				
				// Remove popovers
				$('.btn-tip').popover('destroy');

				// Build the modal
				self.buildModal();
				$('body').append(self.modal);

				// Get all the button tips
				var $textTips = $('.btn-tip');
				$textTips.bind('click', function(){
					self.modal.find('.modal-inner').html($(this).attr('data-content'));
					$('.text-tip-modal').modal('show')
				});				
			} else {
				// Unbind any Modals
				if (self.configs.hasLoaded === true){
					$('.btn-tip').unbind();
					self.removeModal();
				}

				// Setup Popovers
				$('.btn-tip').popover({
					 'trigger': 'click | hover',
					 'placement': 'auto left',
					 delay: { 
					 	'show': 50, 
					 	'hide': 250
					 }
				});				
			}			
		};
		self.positionTips();
		self.configs.hasLoaded = true;
		self.configs.currentFormat = self.getFormat();

		// Expose methods / properties
		return {
			positionTips: self.positionTips
		};
	};

	// Setup the global object on document ready
	$(function(){ 
		window.textTips = new textTips(jQuery); 
		if (window.textTips !== false){
			$('body').bind('breakpointHit', window.textTips.positionTips);	
		}
	});

})(jQuery);