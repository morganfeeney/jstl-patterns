// Text Toggle
(function($) {
	var textToggle = function ($){
		var self = this;

		// Find any toggle texts and setup events
		self.init = function (){
			// Find all the elements with the text toggle data attribute
			var elems = $('[data-text-toggle]');

			elems.each(function (){			
				$(this).on('click', function (){
					if ($(this).hasClass('clicked')) return;
					$(this).addClass('clicked');					

					var currentText = $(this).text();
					$(this).text($(this).attr('data-text-toggle'))
					$(this).attr('data-text-toggle', currentText);

					self.setActive($(this));
				});
			});
		};

		self.setActive = function ($elem){
			var timer = setTimeout(function(){ 
				$elem.removeClass('clicked'); 
			}
			, 350);
		};

		self.init();
	};

	// Add text Toggle to the Bootstrap Functions global obj
	$(function(){ window.BootstrapFunctions.textToggle = textToggle($); });
})(jQuery);