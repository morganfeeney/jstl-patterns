$(function(){

    //-------------------------------------------------------------//
    // TO DO: change brand ID to uppercase or lowercase EVERYWHERE //
    //                                                             //
    // The below section is for the brand image replacement        //
    //-------------------------------------------------------------//

    // Set nice name for brand
    var nice_name = (function(){
      var brand;
      switch (jdwuidemo.getCurrentThemeID()){
        case "int":
          brand = "simplybe";
        break;
        case "sbu":
          brand = "simplybe";
        break;
        default:
          brand = "simplybe";
        break;
      }
      return brand;
    })();

    // Replace theme favicons
    $('.favicon-theme').each(function(key,data){
      var img_src = $(this).attr('src');
      var new_src = img_src.replace(/theme-[a-z0-9]+/i, "theme-" + jdwuidemo.getCurrentThemeID());
      new_src = new_src.replace(/common-icon+/i, jdwuidemo.getCurrentThemeID() + "-icon");
      $(this).attr('src', new_src);
    });

    // Replace brand logo
    $('.brand-logo').each(function(key,data){
      var img_src = $(this).attr('src');
      var new_src = img_src.replace(/\/i\/[a-z0-9]+\/[a-z0-9]+/i, "/i/" + nice_name + "/" + jdwuidemo.getCurrentThemeID().toUpperCase());
      $(this).attr('src', new_src);
    })
});

//----------------------------------------------------------//
// The below section is for the collapse/hide functionality //
//----------------------------------------------------------//

// Wait until DOMContentLoaded before doing addAttrClasses()
(function() {
  document.addEventListener('DOMContentLoaded', addAttrClasses);
})();

// Do addAttrClasses() which will add a small banner
// to the top of each pre tag with the language type
function addAttrClasses (){ 
  $('pre[class*="language-"]').each(function(){
    var classname = $(this).attr('class');
    var classesSplit = classname.split('-');
    if (classesSplit[1] === 'markup') classesSplit[1] = 'html';
    $(this).attr('rel', classesSplit[1]).addClass('snippet');
  });
}

(function() {

  /** Scroll to open section **/
  $('section.panel').on('shown.bs.collapse', function(e){
    var targetElemParent = $(e.target).parent();
    if (targetElemParent.is('section.panel')){
      var $tabPanel = $(this)
      $('html, body').animate({
            scrollTop: $tabPanel.offset().top
        }, 350);
        e.preventDefault();
        localStorage.setItem('currentItem', $(targetElemParent).children('.collapse').attr('id'));
    }
  });

  $('section.panel').on('hide.bs.collapse', function(e) {
    localStorage.removeItem('currentItem');
  });
  /** END **/

  /** Open section if URL contains ID **/
    var anchor = window.location.hash.toLowerCase().replace(" ", "-");
    console.log(anchor);
    $(anchor).collapse('show');
    /** END **/
})(); 

/** Load current open section if user switches theme **/
$(window).load(function() {
  if (localStorage.getItem('currentItem') != null) {
    $('#'+ localStorage.getItem('currentItem')).collapse('show');
  }
});