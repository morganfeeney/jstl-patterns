/*
 * jQuery Double Tap
 * Developer: Sergey Margaritov (sergey@margaritov.net)
 * Date: 22.10.2013
 * Based on jquery documentation http://learn.jquery.com/events/event-extensions/
 */
(function($){
  $.event.special.doubletap = {
    bindType: 'touchend',
    delegateType: 'touchend',

    handle: function(event) {
      var handleObj   = event.handleObj,
          targetData  = jQuery.data(event.target),
          now         = new Date().getTime(),
          delta       = targetData.lastTouch ? now - targetData.lastTouch : 0,
          delay       = delay == null ? 200 : delay;

      if (delta < delay && delta > 30) {
        targetData.lastTouch = null;
        event.type = handleObj.origType;
        ['clientX', 'clientY', 'pageX', 'pageY'].forEach(function(property) {
          event[property] = event.originalEvent.changedTouches[0][property];
        })

        // let jQuery handle the triggering of "doubletap" event handlers
        handleObj.handler.apply(this, arguments);
      } else {
        targetData.lastTouch = now;
      }
    }
  };
})(jQuery);

(function ($){
  'use strict';
	var dimensions, productZoom, carousel, thumbs;

	// Configs
	var configs = {
		panzoomConfigs : {
			minScale: 1,
			contain: 'invert',
			disablePan: true
		},
		owlCarouselConfigs: {
			singleItem: true,
			lazyLoad: true,
			touchDrag: true,
			mouseDrag: true,
			navigation: false,
			addClassActive: true,
			responsiveBaseWidth: '#zoom-container'
		},
		videoConfigs: {
			autoplay: false,
            nativeControlsForTouch: false
		}
	};

	// Store the url params
	var urlParams = function () {
		var query_string = {};
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i=0;i<vars.length;i++) {
			var pair = vars[i].split("=");
			if (typeof query_string[pair[0]] === "undefined") {
				query_string[pair[0]] = decodeURIComponent(pair[1]);
			} else if (typeof query_string[pair[0]] === "string") {
				var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
				query_string[pair[0]] = arr;
			} else {
				query_string[pair[0]].push(decodeURIComponent(pair[1]));
			}
		}
		return query_string;
	}();

	// Init zoom
	function init (){

    // Before doing any setup we create a dom element and initiate panzoom then remove it. This
    // is a work around for a jQuery bug on Safari 8
    var tmpElem = $('<div>');
    $('body').append(tmpElem);
    tmpElem.panzoom();
    tmpElem.panzoom('destroy');
    tmpElem.remove();

		// Init everything
		dimensions 		= new Dimensions();
		productZoom 	= new ProductZoom();
		carousel 		= new Carousel(configs.owlCarouselConfigs);
		thumbs 			= new ThumbsCarousel();

		$('.lazyOwl').load(function (){
			$(this).addClass('imgLoaded');
			productZoom.initZoom($(this), true);
		});

		productZoom.initZoomCtrls();

		// Window resize
		$(window).on('orientationchange resize', BootstrapFunctions.debounce(resized, 500));
	}

	// Deal with browser resizing
	function resized (){
		productZoom.destroyPanZoom();

		// Update the container dimensions
		dimensions.setDimensions();

		// Setup the new slide if the image has previously been loaded
		productZoom.reInit();

		// Position the arrows etc
		carousel.owlPositioning();
	}

	// ===========================================
	// DIMENSIONS
	// ===========================================
	function Dimensions (){
		// Private vars
		var top_bar_height, btm_bar_height, container_height, container_width;

		setDimensions();

		function getTopBarHeight (){ return top_bar_height; }
		function getBtmBarHeight (){ return btm_bar_height; }
		function getContainerHeight (){ return container_height; }
		function getContainerWidth (){ return container_width; }

		function setDimensions (){
			top_bar_height 		= $('#jdw-lb-top').height();
			btm_bar_height 		= $('#jdw-lb-btm').height();
			container_height 	= $(window).height() - (top_bar_height + btm_bar_height);
			container_width 	= $(window).width();
			$('#zoom-container').css('height', container_height);
		}

		function setSlideDimensions ($elem){
			var divider = { width: 0, height:0 };

			// Get the divider from the width
			if ($elem[0].naturalWidth > getContainerWidth()){
				divider.width = $elem[0].naturalWidth / getContainerWidth();
			}

			if ($elem[0].naturalHeight > getContainerHeight()){
				divider.height = $elem[0].naturalHeight / getContainerHeight();
			}

			// If either divider have been set find the bigger of the two
			if (divider.width !== 0 || divider.height !== 0){
				// Panzoom seems to work to 5 decimal places so round to 5
				divider = ((divider.width > divider.height) ? divider.width : divider.height).toFixed(5);
			} else {
				// Destroy panzoom if it exists on the element - this is to cover orientation and window size changes
				$elem.panzoom('destroy');
				divider = false;
			}

			// Set the sizes
			if (divider){
				$elem.parent().css('width', Math.round($elem[0].naturalWidth / divider));
				$elem.parent().css('height', Math.round($elem[0].naturalHeight / divider));
			} else {
				// Set the container size to the size of the image
				$elem.parent().css('height', $elem[0].naturalHeight);
				$elem.parent().css('width', $elem[0].naturalWidth);
			}

			return divider;
		}

		return {
			getTopBarHeight:getTopBarHeight,
			getBtmBarHeight:getBtmBarHeight,
			getContainerHeight:getContainerHeight,
			getContainerWidth:getContainerWidth,
			setDimensions:setDimensions,
			setSlideDimensions:setSlideDimensions
		};
	}

	// ===========================================
	// ZOOM
	// ===========================================
	function ProductZoom (){
		var zooming = false;
		var instruction_visible = true;
		var zoom_increment_divider = 10;
		var zoom_btns = {
			"zoom_in" : {
				elem: $('#zoomIn'),
				active: true,
			},
			"zoom_out" : {
				elem: $('#zoomOut'),
				active: false,
			}
		};

		function getZoomInActive (){ return zoom_btns.zoom_in.active; }
		function setZoomInActive (status){
			if (typeof(status) === "boolean") {
				zoom_btns.zoom_in.active = status;
			}
		}

		function getZoomOutActive (){ return zoom_btns.zoom_out.active; }
		function setZoomOutActive (status){
			if (typeof(status) === "boolean") {
				zoom_btns.zoom_out.active = status;
			}
		}

		function removeInstruction (){
			$('.gestureMessage').removeClass('in');
			setTimeout(function(){
				$('.gestureMessage').remove();
			}, 150);
		}

		function addInstruction (instruction_type){
			var instructions = '<li class="pinch"><span>Pinch zoom</span></li><li class="swipe"><span>Swipe image</span></li>';
			if (instruction_type == 'desktop'){
				instructions = '<li class="dblClick"><span>Double click to zoom</span></li><li class="grab"><span>Grab to pan when zoomed</span></li>';
			}
			$('#zoom-container').prepend('<div class="gestureMessage fade in"><ul>' + instructions + '</ul></div>');
		}

		function initZoom ($elem){
			setZoomOutActive(false);

			// Zoom is only applied if the images is not already showing at its largest size
			var max_scale = dimensions.setSlideDimensions($elem);
			if (max_scale === false) {
				setZoomInActive(false);
				productZoom.updateZoomBtns();
				return;
			}

			// Show the gesture instruction
			if (instruction_visible){
				var instruction_type = WURFL.is_mobile ? "mobile" : "desktop";
				addInstruction(instruction_type);
				instruction_visible = false;
				setTimeout(removeInstruction, 3000);
			}

			setZoomInActive(true);
			$elem.addClass('panzoom');

			// temp config obj
			var conf = $.extend({ maxScale: max_scale, increment: (max_scale/zoom_increment_divider) }, configs.panzoomConfigs);

			// Setup zoom
			var zoom_elem = $elem.panzoom(conf);

			// Zoom btn classes
			zoom_elem[0].conf = conf;
			productZoom.updateZoomBtns();

			zoom_elem.on('panzoomzoom', function(e) {
				zooming = true;
				// On zoom end we add a little delay before resetting the zooming value
				// this is to stop false double taps that can sometimes occur when
				// a user removes two fingers from the screen after a pinch
				setTimeout(function(){
					zooming = false;
				}, 500);

				togglePan();
				updateZoomBtnsAfterZoom();
			});

			// Setup zoom events
			zoom_elem.on('mousewheel.focal', function(e) {
				e.preventDefault();
				var delta = e.delta || e.originalEvent.wheelDelta;
				var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
				zoom_elem.panzoom('zoom', zoomOut, {
					increment: 0.5,
					animate: false,
					focal: e
				});
				togglePan();
				updateZoomBtnsAfterZoom();
			});

			zoom_elem.on('doubletap dblclick', function(e) {
				if (zooming) return;
				// Need to enable pan so that focal works
				if (getCurrentZoom() == 1) $('.panzoom').panzoom('enablePan');

				if (getCurrentZoom() === parseFloat(this.conf.maxScale)){
					resetZoom();
				} else {
					zoom_elem.panzoom("zoom", {
						increment: this.conf.maxScale,
						focal: e,
						animate: true
					});
				}
				togglePan();
				updateZoomBtnsAfterZoom();
			});
		}

		function initZoomCtrls (){
			zoom_btns.zoom_in.elem.on('click', function(){
				$('.panzoom').panzoom('zoom', { increment: 1 });
			});
			zoom_btns.zoom_out.elem.on('click', function(){
				$('.panzoom').panzoom('zoom', true, { increment: 1 });
			});
		}

		function updateZoomBtnsAfterZoom (){
			var zoomOut = getCurrentZoom() != 1 ? true : false;
			var zoomIn = getCurrentZoom() != $('.panzoom')[0].conf.maxScale ? true : false;
			productZoom.setZoomOutActive(zoomOut);
			productZoom.setZoomInActive(zoomIn);
			updateZoomBtns();
		}

		function updateZoomBtns (){
			toggleZoomBtn(zoom_btns.zoom_in.elem, productZoom.getZoomInActive());
			toggleZoomBtn(zoom_btns.zoom_out.elem, productZoom.getZoomOutActive());
		}

		function destroyPanZoom () {
			if ($('.panzoom').length > 0){
				// Remove the event listeners
				$('.panzoom').off('doubletap dblclick mousewheel.focal');
				// Destroy the zoom
				$('.panzoom').panzoom('resetZoom', false).panzoom('destroy').removeAttr('style').removeClass('panzoom');
			}
		}

		function resetZoom () {
			$('.panzoom').panzoom("resetZoom", true);
			$('.panzoom').panzoom("resetDimensions");
		}

		function toggleZoomBtn (btn, toggle) {
      if (toggle === false){
        btn.addClass('disabled');
      }
      else {
        btn.removeClass('disabled');
      }
		}

		function togglePan () {
			if (getCurrentZoom() != 1){
				$('.panzoom').panzoom('enablePan');
			} else {
				$('.panzoom').panzoom('disablePan');
			}
		}

		function getCurrentZoom () {
			if ($('.panzoom').length === 0) return false;
			var currentZoom = $('.panzoom').panzoom('getMatrix')[0];
			return parseFloat(currentZoom);
		}

		function reInit () {
			if ($('.owl-item.active').find('.imgLoaded').length > 0){
				initZoom($('.owl-item.active .imgLoaded'), true);
			}
		}

		return {
			reInit : reInit,
			destroyPanZoom : destroyPanZoom,
			updateZoomBtns : updateZoomBtns,
			initZoomCtrls : initZoomCtrls,
			initZoom : initZoom,
			getZoomOutActive : getZoomOutActive,
			setZoomOutActive : setZoomOutActive,
			setZoomInActive : setZoomInActive,
			getZoomInActive : getZoomInActive,
			updateZoomBtnsAfterZoom:updateZoomBtnsAfterZoom
		};
	}

	// ===========================================
	// CAROUSEL
	// ===========================================
	function Carousel (conf){
		// Privates
		var prevBtn, nextBtn;

		// Init the carousel
		var tmpConfigs = $.extend({ afterInit: afterInit, afterMove: sliderMove }, conf);

		// If the slide parameter is not 0 (first slide), we need to reorder the slides
		// before the carousel is setup
		if (urlParams.slide !== '0'){
			// Main slide
			var active_slide = $("#owl-demo .item").eq(Number(urlParams.slide));
			$(active_slide).parent().prepend(active_slide);
			// Thumb
			var active_thumb = $('#thumbs-container li').eq(Number(urlParams.slide));
			$(active_thumb).parent().prepend(active_thumb);
		}

		var owlCarousel = $("#owl-demo").owlCarousel(tmpConfigs);
		setupSlideCounter();

		// Enable btm bar arrows if more than 1 slide
		if (getSlideCountTotal() > 1){
			$('#navLeft, .owl-prev').on('click', function(){ owlCarousel.data('owlCarousel').prev(); });
			$('#navRight, .owl-next').on('click', function(){ owlCarousel.data('owlCarousel').next(); });
		} else {
			$('.owl-next, .owl-prev').remove();
			$('#navLeft, #navRight').addClass('disabled');
		}

		// After owl carousel init
		function afterInit (){
			// Setup the arrow refs
			prevBtn = $('.owl-prev');
			nextBtn = $('.owl-next');

			// Carousel positioning
			owlPositioning();

			// Check if the active slide contains and video and do some slide setup
			// Update the config to autoplay if the video is the active slide on opening zoom
			if (isActiveSlideVideo()) {
				configs.videoConfigs.autoplay = true;
				videoSlideSetup();
			}
			// Sort out any videos
            initVideos();
		}

		// Check if the active slide contains a video
		function isActiveSlideVideo (){
			if ($('.owl-item.active').find('video').length > 0) {
				return true;
			}
			return false;
		}

		// Ready a video slide
		function videoSlideSetup (){
			productZoom.setZoomOutActive(false);
			productZoom.setZoomInActive(false);
		}

		// Fires after every owl carousel move
		function sliderMove (){
			// Reset the zoom on all of the slides
			productZoom.destroyPanZoom();
			// Check if the current slide has an image that has already been loaded
			// and initialised previously with panzoom
			productZoom.reInit();

			// Check if there are any videos playing and stop them
			if ($('.vjs-playing').length > 0){
				// Get the unique ID of the video
				var videoID = $('.vjs-playing').attr('id');
				videojs(videoID).pause();
			}

			// Update the slide counter
			updateSlideCounter();
			if (isActiveSlideVideo()) videoSlideSetup();

			// Handle video slides
			productZoom.updateZoomBtns();

			// Update thumbnails
			thumbs.updateThumbs(owlCarousel.data('owlCarousel').currentItem);
		}

		// Go to a specific slide
		function goTo (index){
			owlCarousel.data('owlCarousel').goTo(index);
		}

		// Setup heights etc for owl carousel elems
		function owlPositioning (){
			positionNavArrows();
			$('#zoom-container .owl-wrapper').css({
				'height' : dimensions.getContainerHeight()
			});
		}

		// Centres the owl carousel arrows
		function positionNavArrows (){
			if (prevBtn !== null){
				var btnOffset = (dimensions.getContainerHeight()/2) - (prevBtn.height()/2);
				prevBtn.css({ 'top' : btnOffset });
				nextBtn.css({ 'top' : btnOffset });
			}
		}

		// Init the counter
		function setupSlideCounter (){
			// Set the total
			$('#jdw-lb-top .slidescount').text(getSlideCountTotal());
			updateSlideCounter();
		}

		// Returns the total amount of slides
		function getSlideCountTotal (){
			var itemCount;
			try {
				itemCount = owlCarousel.data('owlCarousel').itemsAmount;
			}
			catch(err) {
				itemCount = 1;
			}
			return itemCount;
		}

		// Update the current slide number on the counter
		function updateSlideCounter (){
			$('#jdw-lb-top .currentSlide').text(getCurrentSlide());
		}

		// Returns the current slide
		function getCurrentSlide (zeroIndex){
			if (zeroIndex === undefined) zeroIndex = false;
			var currentItem = owlCarousel.data('owlCarousel').currentItem;
			if (zeroIndex !== true) currentItem = currentItem + 1;
			return currentItem;
		}

		// Update owl carousel widths
		function updateCarouselWidths (){
			owlCarousel.data('owlCarousel').updateVars();
		}

        // Finds any videos in the carousel and inits amp for each
        function initVideos(){
            // Find any videos
            var videos = $('.video-wrapper');
            videos.each(function(){
                initAmpVideo($(this));
            });
        }

		// Amplience video init - expects an item as a $ object
		function initAmpVideo ($videoItem){
			var videoPoster = $videoItem.find('.video').attr('poster');
			var img = new Image();
			$(img).load(function(){
				img = null;
				dimensions.setSlideDimensions($videoItem);
				$videoItem.ampVideo(configs.videoConfigs);
			});
			img.src = videoPoster;
		}

		// Expose some methods
		return {
			owlPositioning:owlPositioning,
			updateCarouselWidths:updateCarouselWidths,
			goTo:goTo,
			getCurrentSlide:getCurrentSlide
		};
	}

	// ===========================================
	// THUMBNAILS
	// ===========================================
	function ThumbsCarousel (){
		var animating = false, thumbHeight = 162, thumbnailsMargin = 10, thumbsAnimationDuration = 200;
    var thumbSize = thumbHeight + thumbnailsMargin;

		var thumbsContainer = $('#thumbs-container #zoom-thumbnail-list');
		setupThumbnailEvents();
		setActiveThumbClass(carousel.getCurrentSlide(true));

		// Show thumbnails on load for larger screen sizes
		if (BootstrapFunctions.isBreakpoint(['md', 'lg'])){
			toggleThumbnails();
		}

		// Update thumbnail positions
		function updateThumbs (activeIndex){
      // The offset of the active thumbnail
			var activeThumbPosition = $($('#zoom-thumbnail-list li').get(activeIndex)).position().top;

      // The thumbnails position is relative to it's direct UL container so it will always be positive.
      // We minus the UL containers position to get the thumbnails position relative to our outer
      // thumbnail container.
      if (thumbsContainer.position().top < 0){
        activeThumbPosition = activeThumbPosition - Math.abs(thumbsContainer.position().top);
      }
			setActiveThumbClass(activeIndex);
      updateThumbsPosition(activeThumbPosition);
		}

    // If a direction is set then we know a nav arrow has been used
    function updateThumbsPosition (offset, direction){
      if (direction !== undefined){
        return moveTo(offset, direction);
      }
      return moveToThumb(offset);
    }

    // Thumbnail nav click
    function thumbnailNavClicked(btn){
      var offset = thumbsContainer.position().top;
      var direction = btn.hasClass('thumbs-next-btn') ? 'down' : 'up';
      updateThumbsPosition(offset, direction);
    }

    // Handles thumbnail navigation
    function moveTo (offset, direction){
      var move = moveConfig(),
          thumbListHeight = thumbsContainer.outerHeight(true),
          parentHeight = thumbsContainer.parent().outerHeight(true);

      // If the list isn't long enough for scrolling
      if (thumbListHeight <= parentHeight) return;

      // Up button
      if (direction == 'up'){
        if (offset === 0){
          // We're at the top so ping to the bottom
          move.distance = (thumbListHeight - parentHeight);
        }
        else {
          move.direction = '+';
          // If there is room for a whole thumb
          if (Math.abs(offset) >= thumbSize){
            move.distance = thumbSize;
          }
          // else move by the distance remaining
          else {
            move.distance = Math.abs(offset);
          }
        }
      }
      // Down button
      else if (direction == 'down'){
        var btm_pos = offset + thumbListHeight;
        // We're at the bottom so ping to the top
        if (btm_pos <= parentHeight){
          move.direction = '+';
          move.distance = thumbListHeight - parentHeight;
        }
        else {
          // If there is room for a whole thumb
          if (btm_pos - thumbSize >= parentHeight){
            move.distance = thumbSize;
          }
          // else move by the distance remaining
          else {
            move.distance = Math.abs(parentHeight - btm_pos);
          }
        }
      }
      moveThumbs(move);
    }

    // Moves a specific thumbnail into view
    function moveToThumb (offset){
      var move = moveConfig(),
          parentHeight = thumbsContainer.parent().outerHeight(true);

      if (offset < 0){
        move.direction = '+';
        move.distance = Math.abs(offset - thumbnailsMargin);
      }
      else if ((offset + thumbSize) > parentHeight){
        move.distance = Math.abs((offset + thumbSize) - parentHeight);
      }
      moveThumbs(move);
    }

    // Thumbnail move setup object
    function moveConfig (){
      return {
        distance: 0,
				direction: '-',
      };
    }

    // Thumbnail clicked
		function thumbClicked (thumb){
			carousel.goTo(thumb.index());
		}

		// Generic method that moves the thumbnails up and down
		function moveThumbs (configs){
			if (!animating){
				animating = true;
				$('#zoom-thumbnail-list').css('position', 'relative').animate({
					'top' : configs.direction + '=' + configs.distance
				}, thumbsAnimationDuration, function(){
					animating = false;
				});
			}
		}

    // Toggles active css class on thumbnails
		function setActiveThumbClass(activeIndex){
			$('#zoom-thumbnail-list li.active').removeClass('active');
			$($('#zoom-thumbnail-list li').get(activeIndex)).addClass('active');
		}

    // Setup click and touch events for thumbnails
		function setupThumbnailEvents (){
			// Thumbnail navigation
			$('.thumbs-nav-btn').on('click touch', function (){
				thumbnailNavClicked($(this));
			});
			// Thumbnail clicked
			$(thumbsContainer.find('li')).on('click touch', function (){
				thumbClicked($(this));
			});
			// Toggle thumbs menu
			$('#zoomToggleThumbs').on('click touch', function(){
				toggleThumbnails();
			});
		}

		// Toggle Thumbnail visibility
		function toggleThumbnails (){
			var toggleDirection;
			if (!$('#zoom-outer-container').hasClass('thumbnails-active')){
				toggleDirection = 'opening';
			} else {
				toggleDirection = 'closing';
			}

			$('#zoom-outer-container').toggleClass('thumbnails-active');
			$('.owl-item.active .item').addClass('thumbnails-' + toggleDirection);
			carousel.updateCarouselWidths();
		}

		// Expose some methods
		return {
			updateThumbs: updateThumbs,
			toggleThumbnails: toggleThumbnails
		};
	}

	$(function(){ init(); });
}(jQuery));