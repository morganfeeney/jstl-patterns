(function($) {
	$(document).ready(function(e) {
		$.slidebars({
			siteClose: true,
			//disableOver: 768,
			hideControlClasses: true,
			scrollLock: true
		});
		e.preventDefault();
	});
})

var mySlidebars = new $.slidebars();

$('.refine-filter').on('click', function(e) {
	mySlidebars.slidebars.open('right');
	$('.close-sb-btn').removeClass('hide-filter-btn');
	e.preventDefault();
});

$('.close-sb-btn').on('click', function(e) {
	mySlidebars.slidebars.close();
 	$('.close-sb-btn').delay(400).queue(function(){
	  $(this).addClass('hide-filter-btn');
	});
    e.preventDefault();
});
(jQuery);