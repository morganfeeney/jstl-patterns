module.exports = function(grunt) {
    // Simple config to run jshint any time a file is added, changed or deleted
    grunt.initConfig({
        watch: {
            options: {
                spawn: false,
                livereload: true
            },
            jstl: {
                files: [
                    '*.jsp',
                    'WEB-INF/**/*',
                    '!WEB-INF/lib/**/*'
                ]
            }
        },
    });
    // Load the plugins to run your tasks
require("load-grunt-tasks")(grunt, {
 scope: "devDependencies"
});
require("time-grunt")(grunt);

// Default task(s).
grunt.registerTask("default", [
 "watch"
]);
}
