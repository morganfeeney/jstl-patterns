<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Index page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
  </head>
    <body>
		<div class="jumbotron">
			<div class="container">
				<h1 class="display-3">UI Library</h1>
				<p class="lead">Please select a templates you would like to view...</a></p>
			</div>
	    </div>
	    <div class="container" style="text-transform:capitalize">
		    <div class='list-group m-b-1'>
			    <a class='list-group-item active'>sign in</a>
			    <a class='list-group-item' href='./sign-in.jsp'>Sign in</a>
                <a class='list-group-item' href='./sign-in-forgotten-password.jsp'>Sign in forgotten password</a>
			    <a class='list-group-item' href='./web-enable-part-4.jsp'>Web enable part 4</a>
			</div>
      <div class='list-group m-b-1'>
        <a class='list-group-item active'>PDP examples</a>
        <a class='list-group-item' href='./pdp.jsp'>PDP</a>
      </div>
			<div class='list-group m-b-1'>
				<a class='list-group-item active'>layout examples</a>
        <a class='list-group-item' href='./long-copy.jsp'>Long copy</a>
				<a class='list-group-item' href='./long-copy.jsp'>Long copy</a>
        <a class='list-group-item' href='./my-account.jsp'>My account</a>
			</div>
            <div class='list-group m-b-1'>
                <a class='list-group-item active'>UI molecules</a>
                <a class='list-group-item' href='./ui-molecules.jsp'>UI molecules</a>
            </div>
    	</div>
    </body>
</html>
