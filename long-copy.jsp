<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>
<%@ taglib prefix="partial" tagdir="/WEB-INF/tags/partials" %>
<%@ taglib prefix="molecule" tagdir="/WEB-INF/tags/molecules"%>
<%@ taglib prefix="organism" tagdir="/WEB-INF/tags/organisms" %>

<template:page pageTitle="PDP example" layoutCss="${layoutCss}">

  <layout:default-layout pageTitle="Long copy page" bodyClass="long-copy-page">
  	<style>
  		#main-content .container-max-width {
  			max-width: 800px;
  		}
  	</style>
  	<h1>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</h1>
  	<p class="lead">There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...</p>
  	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel neque eros. Cras mollis fringilla augue, ac eleifend sem accumsan a. Curabitur massa diam, efficitur a maximus sed, ullamcorper a felis. Donec tincidunt enim nulla, ac tempus quam tempor at. Aliquam erat volutpat. Vivamus velit leo, tincidunt nec vehicula sed, commodo vitae mauris. Mauris elementum, augue in maximus hendrerit, neque odio placerat purus, ornare facilisis dolor neque sed purus.</p>
  	<h2>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</h2>
  	<p>Nam scelerisque non elit sed iaculis. Suspendisse est urna, lacinia ut erat id, posuere viverra justo. Aliquam porttitor tortor ut ligula hendrerit aliquam. Fusce pretium tristique molestie. Mauris lacinia eleifend sollicitudin. Donec tellus libero, bibendum at nisi et, auctor placerat augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed eu diam eget elit lobortis blandit vitae eget eros. Aliquam rutrum ante at quam molestie luctus. Suspendisse egestas porta nibh at molestie. Sed bibendum augue vel arcu faucibus eleifend. Curabitur malesuada commodo commodo. Aliquam vel turpis sagittis, fringilla nisi id, consequat diam.</p>
  	<h3>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</h3>
  	<p>Nullam nec mollis erat. Duis posuere id neque in tincidunt. In fermentum diam sit amet nisl ornare, ut commodo diam sagittis. Duis et elit aliquam, fringilla nisl quis, consectetur odio. Nam maximus, risus non pulvinar viverra, massa dolor scelerisque ligula, vel elementum sem velit vel lacus. Aliquam a tincidunt ex. Donec ullamcorper condimentum quam a feugiat.</p>
  	<h4>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</h4>
  	<p>Integer vestibulum hendrerit odio, et venenatis velit fermentum ut. Mauris id nulla nulla. Donec mi felis, hendrerit at est in, ullamcorper posuere orci. Proin blandit enim quam, ut tempus justo pretium a. Nam velit diam, efficitur nec suscipit sit amet, lobortis ultrices libero. Ut dictum ante magna, sit amet fringilla felis rutrum ut. Proin vel dolor erat. Duis semper, tortor sed tempus bibendum, augue purus interdum arcu, eget varius magna nisi ac ipsum. Nunc ullamcorper metus eu purus laoreet ullamcorper. Maecenas orci orci, sagittis vel aliquet id, vestibulum vitae lorem. Sed vitae quam at arcu elementum accumsan. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean nec tortor commodo ex auctor tempus sit amet eget turpis. Aliquam semper, enim in iaculis convallis, enim lectus euismod sapien, eu ultrices lectus erat sed tortor.</p>
  	<h5>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</h5>
  	<p>Maecenas at posuere lacus. Donec finibus vulputate dapibus. Donec cursus elit nec lacinia pellentesque. Vestibulum tortor sapien, bibendum tempus dictum eget, molestie quis leo. Morbi non magna in erat porttitor malesuada condimentum non lectus. Sed mattis mollis sapien, id ornare purus suscipit ut. Nulla facilisi. Integer pharetra enim nisi.</p>
  </layout:default-layout>

</template:page>
