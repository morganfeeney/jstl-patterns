<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>
<%@ taglib prefix="partial" tagdir="/WEB-INF/tags/partials" %>
<%@ taglib prefix="molecule" tagdir="/WEB-INF/tags/molecules"%>
<%@ taglib prefix="organism" tagdir="/WEB-INF/tags/organisms" %>

<template:page pageTitle="PDP example" layoutCss="${layoutCss}">

  <layout:my-account-layout pageTitle="My account page">
    <div class="well account-details">
      <h3 class="account-subtitle">My Details</h3>
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <div class="panel panel-default">
            <div class="panel-body">
              <h4 class="account-subtitle">Account Address</h4>
              <div class="alert alert-info " role="alert">
                <div class="alert-inner">
                  <p>Your account address will appear here.</p>
                </div>
              </div>

              <p class="adr-stacked">
                <span>
                  <strong>Mr John Smith</strong>
                </span>

                <span class="adr-stacked-inline">
                  <span>Flat no</span>
                  <span>, House Name</span>
                </span>
                <span class="adr-stacked-inline">
                  <span>House no</span>
                  <span>&nbsp;Street</span>
                </span>
                <span>Town/City</span>
                <span>County</span>
                <span>Post Code</span>

                <span>State</span>
                <span>Zip Code</span>

                <span>Locality</span>

                <span>Country</span>
              </p>

              <div class="action-btns">
                <button class="btn btn-secondary account-details-btn" data-toggle="modal" data-target="#editDetailsModal">Edit</button>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-md-6">
          <div class="panel panel-default">
            <div class="panel-body">
              <h4 class="account-subtitle">Contact</h4>

              <p class="adr-stacked clearfix">
                <span class="name">
                  <strong>Email:</strong>
                </span>
                <span>me@myemail.com</span>
              </p>

              <p class="adr-stacked clearfix">
                <span class="name">
                  <strong>Contact Number:</strong>
                </span>
                <span>07712345678</span>
                <span>01234 567891</span>
              </p>

              <div class="alert alert-warning " role="alert">
                <div class="alert-inner">
                  <p>Please add a contact telephone number.</p>
                </div>
              </div>

              <div class="action-btns">
                <button class="btn btn-secondary account-details-btn">Add Contact Number</button>
                <button class="btn btn-secondary account-details-btn">Edit</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </layout:my-account-layout>
</template:page>
