<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>
<%@ taglib prefix="partial" tagdir="/WEB-INF/tags/partials" %>
<%@ taglib prefix="molecule" tagdir="/WEB-INF/tags/molecules"%>
<%@ taglib prefix="organism" tagdir="/WEB-INF/tags/organisms" %>

<%-- This needs to be set in the layout somehow to avoid duplication --%>
<c:set var="layoutCss" value="_ui/responsive/common/css/pdp-layout.css" />

<template:page pageTitle="PDP example" layoutCss="${layoutCss}">

  <layout:pdp-layout>

    <molecule:panel panelType="default" additionalClasses="pdp-product-image">
      <molecule:pdp-product-image imageUrl="p02wv663501w_824d1cb7/WV663"/>
    </molecule:panel>

    <molecule:panel panelType="default" additionalClasses="pdp-product-details">
      <h1>Delivery subscription product title</h1>
      <p>Product details</p>

      <organism:product-details-panel exampleVar="info">
        <p>This is content only for Delivery Subscription PDP</p>
      </organism:product-details-panel>
    </molecule:panel>

  </layout:pdp-layout>
</template:page>
