<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>

<layout:minimal-layout pageTitle="Sign in page" bodyClass="signin layout-single">
    <div class="panel panel-minimal">
        <div class="panel-heading">
            <h3>Forgotten Password</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <form class="col-xs-12 col-sm-9 center-block">
                    <div class="form-group">
                        <p class="text-center">Don’t worry, just enter your email address below and we’ll send you a password reminder email with a link to update it.</p>
                    </div>

                    <div class="form-group has-error">

                        <label for="email" class="control-label required">Email address:</label>

                        <input type="text" id="email" class="form-control" placeholder="Enter email address">

                            <div class="alert alert-danger " role="alert">

                                <div class="alert-inner">Please enter your email address. This email address is not valid.</div>
                            </div>

                        </div>

                        <div class="alert alert-info " role="alert">

                            <div class="alert-inner">The email address provided is connected to more than one retail account. As an additional security measure please provide us with the retail account number for the account you wish to access below.</div>
                        </div>

                        <div class="form-group has-error">
                            <label class="control-label required">Retail account number:</label>
                            <input type="text" class="form-control" id="" placeholder="Enter retail account number">

                                <div class="alert alert-danger " role="alert">

                                    <div class="alert-inner">Please enter your retail account number. The information entered is not valid.</div>
                                </div>

                                <a href="#" class="pull-right link signin-forgotten-acc-no">Forgot your retail account number?</a>
                            </div>

                            <div class="form-group">
                                <div class="center-block-btn">
                                    <button type="button" data-value="submit" class="btn btn-ui-primary btn-block">Send</button>
                                </div>
                            </div>
                            <div class="form-group center-block text-center">
                                <a href="#">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
</layout:minimal-layout>
