<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>

<layout:minimal-layout pageTitle="Sign in page" bodyClass="signin test">
	<div id="signin-accordion">
		<div class="row">
			<div class="col-xs-12 col-phablet-p-6">
				<!-- Existing Customer Form -->
				<div class="panel panel-minimal">
					<div class="panel-heading">
						<h3 class="text-center accordion-heading">Sign In / Existing
							Customer</h3>
					</div>
					<!-- Panel Body -->
					<div class="panel-body clearfix">
						<div class="row">
							<div class="col-xs-12 col-sm-10 col-sm-offset-1">
								<!-- Global form errors -->
								<div class="signin-global-errors">
									<div class="notify notify-error hide">
										<div class="notify-inner">
											Sorry, you can no longer use accounts from other N Brown
											brands to log into Simply Be. Please <a href="#"
												id="signin-create-new-account" data-toggle="collapse"
												data-target="#new-form" role="tab" aria-expanded="false"><u>create
													a new customer account</u></a> to shop at Simply Be
										</div>
									</div>
									<div class="notify notify-error hide">
										<div class="notify-inner">
											An account already exists with this email address. <a
												href="#"><u>Set up this account for online use.</u></a>
										</div>
									</div>
								</div>
								<!--/ Global form errors -->
								<form>
									<div class="alert alert-danger hide">
										<div class="alert-inner">Email address or Password is
											incorrect.</div>
									</div>
									<div class="form-group">
										<label class="control-label required">Email address or
											account number:</label> <input type="email" class="form-control"
											id="" placeholder="Enter email address or account number">
										<div class="notify notify-error hide">
											<div class="notify-inner">Please enter your email
												address or account number. The information entered is not
												valid.</div>
										</div>
									</div>
									<div class="form-group clearfix">
										<label class="control-label required">Password:</label> <input
											type="password" class="form-control" id=""
											placeholder="Enter password"> <a href="#"
											class="link pull-right">Forgot your password?</a>
									</div>
									<div class="center-block-btn">
										<button data-value="signin-existing" type="button"
											value="Sign In"
											class="btn btn-ui-primary btn-sign-in btn-block">Sign
											In</button>
									</div>
								</form>
							</div>
						</div>
						<!--/ Panel Body -->
					</div>
				</div>
				<!--/ Existing Customer Form -->

			</div>
			<div class="col-xs-12 col-phablet-p-6" id="new-customer-signin-form">

				<!-- New Customer Form -->
				<div class="panel panel-minimal">
					<div id="new-head" data-toggle="collapse" data-target="#new-form"
						role="tab" aria-expanded="false">
						<div class="panel-heading">
							<h3 class="accordion-heading">New Customer</h3>
						</div>
						<div class="panel-body clearfix">
							<div class="row">
								<div class="col-xs-12 col-sm-10 col-sm-offset-1">
									<p>Create an account with us today to receive updates and
										make your future shopping easier.</p>
									<div class="center-block-btn">
										<button id="signin-new-customer-btn" type="button"
											class="btn btn-secondary btn-block">New Online
											Customer</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="new-form" class="new-form-collapse collapse clearfix">
						<div class="panel-body clearfix">
							<div class="row">
								<div class="col-xs-12 col-sm-10 col-sm-offset-1">
									<!-- Global form errors -->
									<div class="signin-global-errors">
										<div class="notify notify-error hide">
											<div class="notify-inner">
												An account already exists with this email address. <a
													href="#"><u>Set up this account for online use.</u></a>
											</div>
										</div>
									</div>
									<!--/ Global form errors -->
									<form>
										<div class="alert alert-danger hide">
											<div class="alert-inner">The email address provided
												already exists. The email addresses provided do not match.</div>
										</div>

										<div class="form-group">
											<label class="control-label required" for="titleCode">Title:</label>
											<select id="titleCode" name="titleCode" class="form-control">
												<option value="">Please select a title</option>
												<option value="Mr">Mr</option>
												<option value="Miss">Miss</option>
												<option value="Mrs">Mrs</option>
												<option value="Ms">Ms</option>

											</select>

										</div>


										<div class="form-group">
											<label class="control-label required" for="firstName">First
												name:</label> <input id="firstName" name="firstName" type="text"
												class="form-control" maxlength="15">

										</div>


										<div class="form-group">
											<label class="control-label required" for="lastName">Last
												name:</label> <input id="lastName" type="text" name="lastName"
												class="form-control" maxlength="30">

										</div>


										<!-- DOB -->

										<div class="form-group form-dob">
											<div class="date-selector clearfix">
												<label class="control-label required" for="">Date of
													birth (dd/mm/yyyy):</label> <label class="control-label sr-only"
													for="dob-date-day">Day:</label> <select name="day"
													class="form-control" id="dob-date-day" min="1" max="31">
													<option value="-1" selected="selected" disabled="">Day</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12</option>
													<option value="13">13</option>
													<option value="14">14</option>
													<option value="15">15</option>
													<option value="16">16</option>
													<option value="17">17</option>
													<option value="18">18</option>
													<option value="19">19</option>
													<option value="20">20</option>
													<option value="21">21</option>
													<option value="22">22</option>
													<option value="23">23</option>
													<option value="24">24</option>
													<option value="25">25</option>
													<option value="26">26</option>
													<option value="27">27</option>
													<option value="28">28</option>
													<option value="29">29</option>
													<option value="30">30</option>
													<option value="31">31</option>
												</select> <span class="select-divider text-center">/</span> <label
													class="control-label sr-only" for="dob-date-month">Month</label>
												<select name="month" class="form-control"
													id="dob-date-month" min="1" max="12">
													<option value="-1" selected="selected" disabled="">Month</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12</option>
												</select> <span class="select-divider text-center">/</span> <label
													class="control-label sr-only" for="dob-date-year">Year</label>
												<select name="year" class="form-control" id="dob-date-year"
													min="0" max="9999">
													<option selected="selected" disabled="">Year</option>
													<option value="1">1901</option>
													<option value="2">1902</option>
													<option value="3">1903</option>
													<option value="4">1904</option>
													<option value="5">1905</option>
													<option value="6">1906</option>
													<option value="7">1907</option>
													<option value="8">1908</option>
													<option value="9">1909</option>
													<option value="10">1910</option>
													<option value="11">1911</option>
													<option value="12">1912</option>
												</select>



											</div>
										</div>

										<!--/ DOB -->


										<div class="form-group">
											<label class="control-label required" for="email">Email
												address:</label> <input id="email" name="email" type="email"
												class="form-control" maxlength="250">

										</div>


										<div class="form-group">
											<label class="control-label required" for="email">Re-type
												email:</label> <input id="email" name="email" type="email"
												class="form-control" maxlength="250">

										</div>


										<div class="form-group">
											<label class="control-label required" for="pwd">Password:</label>
											<input id="pwd" name="pwd" type="password"
												class="form-control"
												placeholder="Please enter your password"
												data-toggle="password" maxlength="20">

											<div class="text-tip-outer collapse" aria-expanded="false"
												style="height: 0px;">
												<div class="text-tip">
													<div class="tip-top">
														<p>Please choose a password between 6 and 20
															characters with no spaces, using letters, numbers, and
															special characters. It must not include &lt; &gt; or #.
															Your password is case sensitive.</p>
													</div>
												</div>
											</div>
											<div class="custom-checkbox">
												<input id="show-password" type="checkbox"> <label
													for="show-password">Show Password</label>
											</div>
										</div>


										<!-- Marketing Prefs Container -->
										<div class="marketing-prefs-container">
											<!-- Marketing Prefs Item -->
											<div class="form-group marketing-prefs">
												<div class="panel panel-default marketing-prefs-main">
													<div class="panel-body">
														<div class="checkbox">
															<label for="marketing-prefs-1"> <input
																id="marketing-prefs-1" type="checkbox"
																class="marketing-primary-checkbox"> Send me
																updates from Simply Be
															</label>
														</div>
													</div>
												</div>
												<div class="panel panel-default marketing-prefs-sub">
													<div class="panel-body">
														<div class="row">
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-1a"> <input
																	id="marketing-prefs-1a" type="checkbox"> Email
																</label>
															</div>
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-1b"> <input
																	id="marketing-prefs-1b" type="checkbox"> Post
																</label>
															</div>
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-1c"> <input
																	id="marketing-prefs-1c" type="checkbox"> Phone
																</label>
															</div>
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-1d"> <input
																	id="marketing-prefs-1d" type="checkbox"> SMS
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!--/ Marketing Prefs Item -->
											<!-- Marketing Prefs Item -->
											<div class="form-group marketing-prefs">
												<div class="panel panel-default marketing-prefs-main">
													<div class="panel-body">
														<div class="checkbox">
															<label for="marketing-prefs-2"> <input
																id="marketing-prefs-2" type="checkbox"
																class="marketing-primary-checkbox"> Send me
																updates from other N Brown brands <a
																class="more-text-link" id="marketing-more-0" role="tab"
																data-toggle="collapse" href="#marketing-collapse-0"
																aria-expanded="true"
																aria-controls="marketing-collapse-0"></a>
															</label>
														</div>
													</div>
												</div>
												<!-- Marketing Prefs Info -->
												<div class="row marketing-prefs-info">
													<div class="col-xs-12">
														<div id="marketing-collapse-0" class="collapse"
															role="tabpanel" aria-labelledby="marketing-more-0"
															aria-expanded="false">
															<div class="alert alert-info" role="alert">
																<div class="alert-inner">Lorem ipsum dolor sit
																	amet, consectetur adipisicing elit, sed do eiusmod
																	tempor incididunt ut labore et dolore magna aliqua. Ut
																	enim ad minim veniam, quis nostrud exercitation ullamco
																	laboris nisi ut aliquip ex ea commodo consequat.</div>
															</div>
														</div>
													</div>
												</div>
												<!--/ Marketing Prefs Info -->
												<div class="panel panel-default marketing-prefs-sub">
													<div class="panel-body">
														<div class="row">
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-2a"> <input
																	id="marketing-prefs-2a" type="checkbox"> Email
																</label>
															</div>
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-2b"> <input
																	id="marketing-prefs-2b" type="checkbox"> Post
																</label>
															</div>
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-2c"> <input
																	id="marketing-prefs-2c" type="checkbox"> Phone
																</label>
															</div>
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-2d"> <input
																	id="marketing-prefs-2d" type="checkbox"> SMS
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!--/ Marketing Prefs Item -->
											<!-- Marketing Prefs Item -->
											<div class="form-group marketing-prefs">
												<div class="panel panel-default marketing-prefs-main">
													<div class="panel-body">
														<div class="checkbox">
															<label for="marketing-prefs-3"> <input
																id="marketing-prefs-3" type="checkbox"
																class="marketing-primary-checkbox"> Send me
																updates from specially selected 3rd parties <a
																class="more-text-link" id="marketing-more-1" role="tab"
																data-toggle="collapse" href="#marketing-collapse-1"
																aria-expanded="true"
																aria-controls="marketing-collapse-1"></a>
															</label>
														</div>
													</div>
												</div>
												<!-- Marketing Prefs Info -->
												<div class="row marketing-prefs-info">
													<div class="col-xs-12">
														<div id="marketing-collapse-1" class="collapse"
															role="tabpanel" aria-labelledby="marketing-more-1"
															aria-expanded="false">
															<div class="alert alert-info" role="alert">
																<div class="alert-inner">Lorem ipsum dolor sit
																	amet, consectetur adipisicing elit, sed do eiusmod
																	tempor incididunt ut labore et dolore magna aliqua. Ut
																	enim ad minim veniam, quis nostrud exercitation ullamco
																	laboris nisi ut aliquip ex ea commodo consequat.</div>
															</div>
														</div>
													</div>
												</div>
												<!--/ Marketing Prefs Info -->
												<div class="panel panel-default marketing-prefs-sub">
													<div class="panel-body">
														<div class="row">
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-3a"> <input
																	id="marketing-prefs-3a" name="marketing-prefs-3a"
																	type="checkbox"> Email
																</label>
															</div>
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-3b"> <input
																	id="marketing-prefs-3b" name="marketing-prefs-3b"
																	type="checkbox"> Post
																</label>
															</div>
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-3c"> <input
																	id="marketing-prefs-3c" name="marketing-prefs-3c"
																	type="checkbox"> Phone
																</label>
															</div>
															<div class="checkbox col-xs-6 col-md-3">
																<label for="marketing-prefs-3d"> <input
																	id="marketing-prefs-3d" name="marketing-prefs-3d"
																	type="checkbox"> SMS
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!--/ Marketing Prefs Item -->
										</div>
										<!--/ Marketing Prefs Container -->

										<div class="form-group">
											<div class="center-block-btn">
												<button data-value="signin-new" type="button"
													value="Sign In" class="btn btn-secondary btn-block">Continue</button>
											</div>
										</div>
										<div class="form-group">
											<p>
												By continuing you agree to our <a href="#"><u>Terms
														&amp; Conditions</u></a>
											</p>
										</div>
									</form>
								</div>
							</div>
						</div>
						<!--/ Panel Body -->
					</div>
				</div>
				<!--/ New Customer Form -->

			</div>
		</div>
	</div>
</layout:minimal-layout>
