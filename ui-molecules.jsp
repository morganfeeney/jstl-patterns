<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/templates" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>
<%@ taglib prefix="partial" tagdir="/WEB-INF/tags/partials" %>
<%@ taglib prefix="molecule" tagdir="/WEB-INF/tags/molecules"%>
<%@ taglib prefix="organism" tagdir="/WEB-INF/tags/organisms" %>

<template:page pageTitle="PDP example" layoutCss="${layoutCss}">

  <layout:default-layout pageTitle="UI molecules">

    <section id="alerts">
        <h3>Alerts</h3>

        <molecule:alert alertType="info">
            <p>I'm an alert</p>
        </molecule:alert>

        <molecule:alert alertType="danger">
            <p>I'm an alert too</p>
        </molecule:alert>

        <molecule:alert alertType="warning">
            <p>Look ma, I'm an alert too!</p>
        </molecule:alert>

        <molecule:alert alertType="success">
            <p>Hey, don't forget meeee!</p>
        </molecule:alert>

        <h4>Dismissable alert</h4>

        <molecule:alert alertType="info" dismissable="true">
            <p>I'm an alert, dismiss me!</p>
        </molecule:alert>
    </section>

    <section id="panels">

        <h3>Panels</h3>

        <molecule:panel panelType="default">
            <p>I'm a panel</p>
        </molecule:panel>

        <h4>Panel with heading</h4>

        <molecule:panel panelType="default" panelHeading="Default">
            <p>I'm a panel, here's some content</p>
        </molecule:panel>

        <molecule:panel panelType="success" panelHeading="Success">
            <p>I'm a panel, here's some content</p>
        </molecule:panel>

        <molecule:panel panelType="info" panelHeading="Info">
            <p>I'm a panel, here's some content</p>
        </molecule:panel>

        <molecule:panel panelType="danger" panelHeading="Danger">
            <p>I'm a panel, here's some content</p>
        </molecule:panel>

        <molecule:panel panelType="warning" panelHeading="Warning">
            <p>I'm a panel, here's some content</p>
        </molecule:panel>

        <h4>Collapsible panel</h4>

        <molecule:panel panelType="default" panelHeading="Default" collapsable="true" id="1">
            <p>I'm a panel, here's some content</p>
        </molecule:panel>

    </section>

    <section id="forms">
        <molecule:panel panelType="default">
            <form>
                <molecule:form-group hasError="true">
                    <div class="form-group">
                        <label for="test-1">Test label</label>
                        <input id="test-1" class="form-control" type="text" name="" value="test">
                    </div>
                    <molecule:input inputType="text"/>
                </molecule:form-group>
            </form>
        </molecule:panel>
    </section>
    <%-- <c:forEach begin="0" end="4" varStatus="loop">
        <p>I'm in a looooop ${loop.index}</p>
    </c:forEach> --%>
  </layout:default-layout>
</template:page>
