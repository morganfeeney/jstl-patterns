<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layouts" %>

<layout:minimal-layout pageTitle="Example page" bodyClass="layout-single">
   	<div class="panel panel-minimal">
		<div class="panel-heading">
		  	<h3 class="text-center">New Online Customer</h3>
	  	</div>
		<div class="panel-body">
			<div class="row">
				<div class="center-block">
					<div class="text-center">
						<p>You can now use your Account online.</p>
					</div>
					<div class="center-block-btn">
						<button type="button" data-value="submit" class="btn btn-secondary btn-block">Go to My Account</button>
						<button type="button" data-value="submit" class="btn btn-ui-primary btn-block">Continue Shopping</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</layout:minimal-layout>
